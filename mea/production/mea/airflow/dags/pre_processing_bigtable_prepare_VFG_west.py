import datetime
import os
import uuid

from airflow import DAG
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

#name_convention  Continual_<customer>_<controller_dag_id>
global_cluster_name= 'processing-1-full-day-west'

general_params={
    "ENRICHMENT_JAR": "gs://vf-de-nwp-live-prodb/jars/processing-enrichment-5.6-RC8-jar-with-dependencies.jar", 
    "MOBILITY_JAR" : "gs://vf-de-nwp-live-prodb/jars/processing-mobility-5.6-RC8-gcp-jar-with-dependencies.jar", 
    "AGGS_JAR" : "gs://vf-de-nwp-live-prodb/jars/processing-aggs-5.6-RC8-jar-with-dependencies.jar", 
    "ENRICHMENT_MAIN_CLASS" : "com.continual.sonata.Processing.Enrichment.Host.Host",
    "MOBILITY_MAIN_CLASS" : "com.continual.sonata.Mobility.Processing.Host.Host",
    "AGGS_MAIN_CLASS" : "com.continual.sonata.processing.host.Host",
    "MOBILITY_PRE_PROCESS_MAIN_CLASS" : "com.continual.sonata.Mobility.PreProcessing.Host.Host",
    "CLUSTER_NAME" : "continual-mea-cluster-{{ ds_nodash }}",
    "customer" : "de_west",
    "home_folder" : "gs://vf-de-nwp-live-prodb//",
    "auto_delete_duration": 14400,
    "flow_configuration_file" : "data/configuration.json",
    "composer_bucket_name" : "gs://vf-de-nwp-live-prodb",
    "working_day":"2021-07-20"
}

default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': "vf-de-nwp-live"
}

with DAG(
        "pre_processing_bigtable_prepare_VFG_west",
        start_date=datetime.now() - timedelta(days=1),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
    
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters create """ + global_cluster_name + """ \
            --bucket vf-de-nwp-live-prodb \
            --region europe-west3 \
            --zone europe-west3-b \
            --tags allow-internal-dataproc-prodb,allow-ssh-from-management-zone-prodb \
            --project vf-de-nwp-live \
            --subnet projects/vf-de-nwp-live/regions/europe-west3/subnetworks/prodb-restricted-zone \
            --service-account vf-de-nwp-prodb-dp-ops-sa@vf-de-nwp-live.iam.gserviceaccount.com \
            --metadata enable-oslogin=true \
            --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
            --enable-component-gateway \
            --no-address \
            --master-machine-type n1-standard-8 \
            --worker-machine-type n1-standard-32 \
            --num-workers 16 \
            --max-idle 1h \
            --image-version 1.3-debian10 \
            """
    )
    pre_processing_map = BashOperator(
        task_id='pre_processing_map',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
            --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
            --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=16 \
            -- customer="""+ str(general_params['customer']) + """ \
             flow=map_prepare \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=00:00 \
             job_date=""" + str(general_params['working_day']) + """ \
             type=pre_processing_map \
             gcp=true 
        """
    )
    pre_cell_candidates = BashOperator(
        task_id='pre_cell_candidates',
        bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
                --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=16 \
                -- customer=""" + str(general_params['customer']) + """ \
                 flow=map_prepare \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=01:00 \
                 job_date=""" + str(general_params['working_day']) + """ \
                 type=pre_processing_cell_candidates \
                 gcp=true 
            """
    )
    pre_processing_transitions = BashOperator(
        task_id='pre_processing_transitions',
        bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
                --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=16 \
                -- customer=""" + str(general_params['customer']) + """ \
                 flow=map_prepare \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=01:00 \
                 job_date=""" + str(general_params['working_day']) + """ \
                 type=pre_processing_transitions \
                 gcp=true 
            """
    )
    delete_dataproc_cluster = BashOperator(
        task_id='delete_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + """ \
            --region europe-west3 \
            """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )
    create_dataproc_cluster >> pre_processing_map >> pre_cell_candidates >> pre_processing_transitions>> delete_dataproc_cluster