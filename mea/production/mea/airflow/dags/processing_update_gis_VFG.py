import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
spark_properties = """spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8, spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version='2'"""



default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

with DAG(
        "processing_update_gis",
        start_date=datetime.now() - timedelta(days=1),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
                gcloud beta dataproc clusters create """ + global_cluster_name + """ \
                --bucket vf-de-nwp-live-prodb \
                --region europe-west3 \
                --zone """ + str(global_params['gcp_zone']) + """ \
                --tags allow-internal-dataproc-prodb,allow-ssh-from-management-zone-prodb \
                --project """ + str(global_params['gcp_project']) + """ \
                --subnet projects/vf-de-nwp-live/regions/europe-west3/subnetworks/prodb-restricted-zone \
                --service-account vf-de-nwp-prodb-dp-ops-sa@vf-de-nwp-live.iam.gserviceaccount.com \
                --metadata enable-oslogin=true \
                --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
                --enable-component-gateway \
                --no-address \
                --master-machine-type n1-standard-8 \
                --worker-machine-type n1-standard-32 \
                --worker-boot-disk-size 100 \
                --num-workers 8 \
                --max-idle 1h \
                --image-version 1.3-debian10 \
                """
    )

    update_east = BashOperator(
        task_id='update_east',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
            --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
            --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8 \
            -- customer=de_east \
             flow=update_east \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
             job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
             type=update_gis \
             gcp=true 
        """
    )
    update_west = BashOperator(
        task_id='update_west',
        bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
                --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8 \
                -- customer=de_west \
                 flow=update_west \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                 job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                 type=update_gis \
                 gcp=true 
            """
    )

    delete_dataproc_cluster = BashOperator(
        task_id='delete_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + """ \
            --region europe-west3 \
        """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )



    # first_step >> time_shift_calc >> create_dataproc_cluster >> enrichment_job1 >> enrichment_job2 >> prepare_routes >> routes >> counters >> routes_counters >> delete_dataproc_cluster >> final_step
    # first_step >> create_dataproc_cluster >> delete_dataproc_cluster >> final_step
    create_dataproc_cluster >> update_east >> update_west >> delete_dataproc_cluster
