import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from subdag.calibrate_process_subdag import dag_process
from airflow.operators.subdag_operator import SubDagOperator
from airflow.utils.dates import days_ago

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
spark_properties = 'spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8'

DATAPROC_SPARK_PROP = {
    'spark.executor.instances': '3',
    'spark.executor.memoryOverhead': '4g',
    'spark.executor.cores': '3',
    'spark.executor.memory': '10g',
    'spark.master': 'yarn'
}  # Dict mentioning Spark job's properties


def end_task_clean():
    # set finish Dag execution
    Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    # update dad execution stratus, if one task failed mark the dag as failed
    # current_dag=get_dag()
    # current_dag.update_state()


def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

with DAG(
        "calibrate_process_VFG",
        start_date=days_ago(1),
        schedule_interval=None,
        concurrency=64,
        catchup=False,
        default_args=default_dag_args)as dag:

    final_step = PythonOperator(
        task_id='final_step',
        provide_context=False,
        python_callable=end_task_clean,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )


    first_step = PythonOperator(
        task_id='first_step',
        provide_context=False,
        python_callable=begin_tasks
    )

    region = str(general_params['customer'])
    region_list = region.split(",")
    for customer in region_list:
        args = {
            'customer_name': customer
        }
        dynamic = SubDagOperator(
            task_id=customer,
            subdag=dag_process('calibrate_process_VFG', customer, args)
        )
        first_step.set_downstream(dynamic)
        dynamic.set_downstream(final_step)

