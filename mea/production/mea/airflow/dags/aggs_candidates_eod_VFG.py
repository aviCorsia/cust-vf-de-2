import datetime
import os
import uuid

from airflow import DAG
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from subdag.aggs_candidates_eod_subdag_VFG import dag_process
from airflow.operators.subdag_operator import SubDagOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
spark_properties = 'spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8'


def end_task_clean():
    # set finish Dag execution
    Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    # update dad execution stratus, if one task failed mark the dag as failed,
    # current_dag=get_dag()
    # current_dag.update_state()


def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


yesterday = datetime.combine(
    datetime.today() - timedelta(1),
    datetime.min.time())

default_dag_args = {
    # Setting start date as yesterday starts the DAG immediately when it is
    # detected in the Cloud Storage bucket.
    # 'start_date': yesterday,
    # To email on failure or retry set 'email' arg to your email and enable
    # emailing here.
    'email_on_failure': False,
    'email_on_retry': False,
    # If a task fails, retry it once after waiting at least 5 minutes
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

with DAG(
        "aggs_candidates_eod_VFG",
        start_date=datetime.strptime(str(Variable.get(param_prefix_name + '_' + "working_day")), "%Y-%m-%d"),
        schedule_interval=None,
        concurrency=32,
        default_args=default_dag_args)as dag:

    final_step = PythonOperator(
        task_id='final_step',
        provide_context=False,
        python_callable=end_task_clean
    )

    first_step = PythonOperator(
        task_id='first_step',
        provide_context=False,
        python_callable=begin_tasks
    )
    region = str(general_params['customer'])
    region_list = region.split(",")
    for customer in region_list:
        args2 = {
            'customer_name': customer
        }
        dynamic = SubDagOperator(
            task_id=customer,
            subdag=dag_process('aggs_candidates_eod_VFG', customer, args2)
        )
        first_step.set_downstream(dynamic)
        dynamic.set_downstream(final_step)

    # create_dataproc_cluster >> run_map_job >> run_cell_candidates_job >> run_transitions_job >> delete_dataproc_cluster