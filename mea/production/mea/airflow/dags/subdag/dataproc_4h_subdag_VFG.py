import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
# spark_properties = 'spark.executor.memoryOverhead=2g,spark.executor.memory=19g,spark.executor.cores=5,spark.driver.memory=19g,spark.driver.cores=5,spark.executor.instances=119,spark.default.parallelism=1190'
# spark_properties = 'spark.executor.memoryOverhead=2g,spark.executor.memory=11g,spark.executor.cores=3,spark.driver.memory=11g,spark.driver.cores=3,spark.executor.instances=200,spark.default.parallelism=1200'
# spark_properties = 'spark.executor.memoryOverhead=3g,spark.executor.memory=29g,spark.executor.cores=8,spark.driver.memory=29g,spark.driver.cores=8,spark.executor.instances=79,spark.default.parallelism=1264'

yarn_properties = ",yarn.nodemanager.vmem-check-enabled=false,yarn.nodemanager.pmem-check-enabled=false"
java_properties = ",spark.executor.extraJavaOptions=\"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='kill -9 %p'\""
spark_properties = 'spark.executor.memoryOverhead=10g,spark.executor.cores=20,spark.executor.memory=80g,spark.executor.instances=20' # + yarn_properties + java_properties

def end_task_clean():
    pass
    # set finish Dag execution
    # Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    # update dad execution stratus, if one task failed mark the dag as failed
    # current_dag=get_dag()
    # current_dag.update_state()


def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

def dag_process(parent_dag_name, child_dag_name, args):
    with DAG(
            dag_id='%s.%s' % (parent_dag_name, child_dag_name),
            start_date=days_ago(1),
            # start_date=datetime.strptime(str(Variable.get(param_prefix_name + '_' + "working_day")), "%Y-%m-%d"),
            schedule_interval=None,
            # concurrency=80,
            # catchup=False,
            default_args=default_dag_args)as dag_subdag:
        # create_dataproc_cluster = BashOperator(
            # task_id='create_dataproc_cluster',
            # bash_command="""
                    # gcloud beta dataproc clusters create """ + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                    # --bucket vf-de-nwp-live-prodb \
                    # --region europe-west3 \
                    # --zone """ + str(global_params['gcp_zone']) + """ \
                    # --tags allow-internal-dataproc-prodb,allow-ssh-from-management-zone-prodb \
                    # --project """ + str(global_params['gcp_project']) + """ \
                    # --subnet projects/vf-de-nwp-live/regions/europe-west3/subnetworks/prodb-restricted-zone \
                    # --service-account vf-de-nwp-prodb-dp-ops-sa@vf-de-nwp-live.iam.gserviceaccount.com \
                    # --metadata enable-oslogin=true \
                    # --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
                    # --enable-component-gateway \
                    # --no-address \
                    # --master-machine-type n1-standard-16 \
                    # --master-boot-disk-size 400 \
                    # --worker-machine-type n1-standard-32 \
                    # --worker-boot-disk-size 100 \
                    # --num-workers 20 \
                    # --max-idle 1h \
                    # --image-version 1.3-debian10 \
                    # """
        # )

        enrichment_job1 = BashOperator(
            task_id='spark_enrichment_job1',
            bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['ENRICHMENT_JAR']) + """ \
                --class=""" + str(general_params['ENRICHMENT_MAIN_CLASS']) + """ \
                --properties=""" + spark_properties + """ \
                -- customer=""" + args['customer_name'] + """ \
                 flow=enrichment \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=enrichment_job1_""" + str(uuid.uuid4()) + """ \
                 job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                 job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                 type=enrichment_batch \
                 gcp=true 
            """,
            trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        )
        # job time + 2 hours
        enrichment_job2 = BashOperator(
            task_id='spark_enrichment_job2',
            bash_command="""
                    gcloud dataproc jobs submit spark \
                    --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                    --region=europe-west3 \
                    --jars=""" + str(general_params['ENRICHMENT_JAR']) + """ \
                    --class=""" + str(general_params['ENRICHMENT_MAIN_CLASS']) + """ \
                    --properties=""" + spark_properties + """ \
                    -- customer=""" + args['customer_name'] + """ \
                     flow=enrichment \
                     home_folder=""" + str(general_params['home_folder']) + """ \
                     job_id=enrichment_job2_""" + str(uuid.uuid4()) + """ \
                     job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_shift_internal')) + """ \
                     job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                     type=enrichment_batch \
                     gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )

        prepare_routes = BashOperator(
            task_id='spark_prepare_routes',
            bash_command="""
                    gcloud dataproc jobs submit spark \
                    --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                    --region=europe-west3 \
                    --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                    --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
                    --properties=""" + spark_properties + """ \
                    -- customer=""" + args['customer_name'] + """ \
                     flow=routes \
                     home_folder=""" + str(general_params['home_folder']) + """ \
                     job_id=prepare_routes_""" + str(uuid.uuid4()) + """ \
                     job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                     job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                     type=prepare_routes \
                     gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )
        routes = BashOperator(
            task_id='spark_routes',
            bash_command="""
                    gcloud dataproc jobs submit spark \
                    --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                    --region=europe-west3 \
                    --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                    --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
                    --properties=""" + spark_properties + """ \
                    -- customer=""" + args['customer_name'] + """ \
                     flow=routes \
                     home_folder=""" + str(general_params['home_folder']) + """ \
                     job_id=routes_""" + str(uuid.uuid4()) + """ \
                     job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                     job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                     type=routes \
                     gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )

        counters = BashOperator(
            task_id='spark_counters',
            bash_command="""
                    gcloud dataproc jobs submit spark \
                    --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") +  """ \
                    --region=europe-west3 \
                    --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                    --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
                    --properties=""" + spark_properties + """ \
                    -- customer=""" + args['customer_name'] +""" \
                     flow=routes \
                     home_folder=""" + str(general_params['home_folder']) + """ \
                     job_id=counters_""" + str(uuid.uuid4()) + """ \
                     job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                     job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                     type=counters \
                     gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )
        routes_counters = BashOperator(
            task_id='spark_routes_counters',
            bash_command="""
                    gcloud dataproc jobs submit spark \
                    --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                    --region=europe-west3 \
                    --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                    --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
                    --properties=""" + spark_properties + """ \
                    -- customer=""" + args['customer_name'] + """ \
                     flow=routes \
                     home_folder=""" + str(general_params['home_folder']) + """ \
                     job_id=routes_counters_""" + str(uuid.uuid4()) + """ \
                     job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                     job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                     type=road_counters \
                     gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )

        # delete_dataproc_cluster = BashOperator(
            # task_id='delete_dataproc_cluster',
            # bash_command="""
                # gcloud beta dataproc clusters delete  """ + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                # --region europe-west3 \
            # """,
            # trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        # )
        enrichment_job1 >> enrichment_job2 >> prepare_routes >> routes >> counters >> routes_counters
        return dag_subdag