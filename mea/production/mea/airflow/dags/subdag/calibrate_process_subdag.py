import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago


customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
# 10-07-21 set the number of executors and workers to 5 to accomodate 8 regions (Orio)
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name_var = str(general_params['CLUSTER_NAME'])

yarn_properties = ",yarn.nodemanager.vmem-check-enabled=false,yarn.nodemanager.pmem-check-enabled=false"
java_properties = ",spark.executor.extraJavaOptions=\"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='kill -9 %p'\""
spark_properties = 'spark.yarn.executor.memoryOverhead=2g,spark.executor.memory=19g,spark.executor.cores=5,spark.driver.memory=19g,spark.driver.cores=5,spark.executor.instances=29,spark.default.parallelism=290'

spark_properties = spark_properties + java_properties + yarn_properties

# read runtime date
days_to_subtruct = 0
working_day_str = str(Variable.get(param_prefix_name + '_' + "working_day"))
date_time_str = datetime.strptime(working_day_str, "%Y-%m-%d") - timedelta(days=days_to_subtruct)
working_day = date_time_str.strftime('%Y-%m-%d')
# working_day = str(Variable.get('working_day_temp'))

print("working_day:", working_day)

def end_task_clean():
    # set finish Dag execution
    Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    # update dad execution stratus, if one task failed mark the dag as failed
    # current_dag=get_dag()
    # current_dag.update_state()


def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

# 10-07-21 set the number of executors and workers to 5 to accomodate 8 regions (Orio)
def dag_process(parent_dag_name, child_dag_name, args):
    global_cluster_name = global_cluster_name_var + '-' + child_dag_name.replace("_","-") + "-calibration"

    with DAG(
            dag_id='%s.%s' % (parent_dag_name, child_dag_name),
            start_date=days_ago(1),
            schedule_interval=None,
            # catchup=False,
            default_args=default_dag_args)as dag_subdag:
        create_dataproc_cluster = BashOperator(
            task_id='create_dataproc_cluster',
            bash_command="""
                gcloud beta dataproc clusters create """ + global_cluster_name + """ \
                --bucket vf-de-nwp-live-prodb \
                --region europe-west3 \
                --zone """ + str(global_params['gcp_zone']) + """ \
                --tags allow-internal-dataproc-prodb,allow-ssh-from-management-zone-prodb \
                --project """ + str(global_params['gcp_project']) + """ \
                --subnet projects/vf-de-nwp-live/regions/europe-west3/subnetworks/prodb-restricted-zone \
                --service-account vf-de-nwp-prodb-dp-ops-sa@vf-de-nwp-live.iam.gserviceaccount.com \
                --metadata enable-oslogin=true \
                --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
                --enable-component-gateway \
                --no-address \
                --master-machine-type n1-standard-8 \
                --master-boot-disk-size 200 \
                --worker-machine-type n1-standard-32 \
                --worker-boot-disk-size 100 \
                --num-workers 5 \
                --image-version 1.3-debian10 \
                --max-idle 1h \
                """
        )
                
        # job way
        calibrate_job_way = BashOperator(
            task_id='calibrate_job_way',
            bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['AGGS_JAR']) + """ \
                --class=""" + str(general_params['AGGS_MAIN_CLASS']) + """ \
                --properties=""" + spark_properties + """ \
                -- customer="""+ args['customer_name'] + """ \
                 flow=calibrate \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                 job_date=""" + working_day + """ \
                 type=calibration_way \
                 gcp=true 
            """
        )
        
        # job cell
        calibrate_job_cell = BashOperator(
            task_id='calibrate_job_cell',
            bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['AGGS_JAR']) + """ \
                --class=""" + str(general_params['AGGS_MAIN_CLASS']) + """ \
                --properties=""" + spark_properties + """ \
                -- customer="""+ args['customer_name'] + """ \
                 flow=calibrate \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                 job_date=""" + working_day + """ \
                 type=calibration_cell \
                 gcp=true 
            """
        )
        
        # job way_compare
        calibrate_job_way_compare = BashOperator(
            task_id='calibrate_job_way_compare',
            bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['AGGS_JAR']) + """ \
                --class=""" + str(general_params['AGGS_MAIN_CLASS']) + """ \
                --properties=""" + spark_properties + """ \
                -- customer="""+ args['customer_name'] + """ \
                 flow=calibrate \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                 job_date=""" + working_day + """ \
                 type=calibration_way_compare \
                 gcp=true 
            """,
            trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        )
        
        # job cell_compare
        calibrate_job_cell_compare = BashOperator(
            task_id='calibrate_job_cell_compare',
            bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['AGGS_JAR']) + """ \
                --properties=""" + spark_properties + """ \
                --class=""" + str(general_params['AGGS_MAIN_CLASS']) + """ \
                -- customer="""+ args['customer_name'] + """ \
                 flow=calibrate \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
                 job_date=""" + working_day + """ \
                 type=calibration_cell_compare \
                 gcp=true 
            """,
            trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        )

        delete_dataproc_cluster = BashOperator(
            task_id='delete_dataproc_cluster',
            bash_command="""
                gcloud beta dataproc clusters delete """ + global_cluster_name + """ \
                --region europe-west3 \
            """,
            trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        )
        
        create_dataproc_cluster >> calibrate_job_way >> calibrate_job_cell >> calibrate_job_way_compare >> calibrate_job_cell_compare >> delete_dataproc_cluster
        # create_dataproc_cluster >> calibrate_job_way >> calibrate_job_cell >> delete_dataproc_cluster
        return dag_subdag