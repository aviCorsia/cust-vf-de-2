
import sys
import pandas as pd
from datetime import datetime,date,timedelta
# Airflow modules
from airflow import DAG, configuration
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.contrib.operators.dataproc_operator import DataprocClusterDeleteOperator
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.operators import bash_operator
from airflow.models import Variable
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from airflow.providers.google.cloud.operators.dataproc import DataprocSubmitJobOperator
from airflow.utils import trigger_rule

 
### -----> dag parameters
# global variable to all gcp dags
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
gcp_project = global_params['gcp_project']

# gis global variable
gis_script_dir = Variable.get(param_prefix_name + '_gis')

default_dag_args = {
    # Setting start date as yesterday starts the DAG immediately when it is
    # detected in the Cloud Storage bucket.
    'start_date': days_ago(1),
    # To email on failure or retry set 'email' arg to your email and enable
    # emailing here.
    'email_on_failure': False,
    'email_on_retry': False,
    # If a task fails, retry it once after waiting at least 5 minutes
    'retries': 0,
    'project_id': gcp_project
}

nowDate = datetime.today().strftime('%Y-%m-%d')
#nowDate = datetime.strptime('2021-08-15','%Y-%m-%d')

# Batch command to create Dataproc cluster
templated_create_command_dataproc = """
gcloud dataproc clusters create gis-dataproc \
--bucket vf-de-nwp-live-prodb \
--region europe-west3 \
--zone europe-west3-b \
--subnet projects/vf-de-nwp-live/regions/europe-west3/subnetworks/prodb-restricted-zone \
--tags allow-internal-dataproc-prodb,allow-ssh-from-management-zone-prodb \
--project vf-de-nwp-live \
--service-account vf-de-nwp-prodb-dp-ops-sa@vf-de-nwp-live.iam.gserviceaccount.com \
--master-machine-type n1-standard-4 \
--max-idle 2h \
--single-node \
--initialization-actions gs://gis_script_dir/download_config_file_to_dataproc.sh \
--metadata enable-oslogin=true \
--properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
--optional-components=ANACONDA,JUPYTER,ZEPPELIN \
--enable-component-gateway \
--image-version 1.5-debian10 \
--no-address \
"""

get_files = """
gcloud dataproc jobs submit pyspark gs://gis_script_dir/get_raw_gis.py \
--cluster=gis-dataproc \
--region=europe-west3 \
-- CHANGE_THE_DAY
"""

run_gis_on_dataproc = """
gcloud dataproc jobs submit pyspark gs://gis_script_dir/vfGermanyGIS.py \
--cluster=gis-dataproc \
--region=europe-west3 
"""


with DAG('gis-dataproc',  start_date=datetime.now(),default_args=default_dag_args, schedule_interval=None) as dag:
    # Create Cloud Dataproc cluster
    create_cluster = bash_operator.BashOperator(
        task_id='create_gis_dataproc',
        bash_command=templated_create_command_dataproc.replace('gis_script_dir',str(gis_script_dir))
    )

    get_files_for_gis = bash_operator.BashOperator(
        task_id='get_files',
        bash_command=get_files.replace('gis_script_dir',str(gis_script_dir)).replace('CHANGE_THE_DAY',str(nowDate))
    )

    run_gis = bash_operator.BashOperator(
        task_id='run_gis',
        bash_command=run_gis_on_dataproc.replace('gis_script_dir',str(gis_script_dir))
    )    

    delete_dataproc_cluster = DataprocClusterDeleteOperator(
        project_id=default_dag_args['project_id'],
        task_id='delete_gis_dataproc',
        cluster_name='gis-dataproc',
        region='europe-west3',
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )


    # Trigger DAG
    create_cluster >> get_files_for_gis >> run_gis >> delete_dataproc_cluster