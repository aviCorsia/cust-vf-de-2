import pprint
import datetime
import json
from airflow.models import DAG
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
from airflow.models import Variable
from airflow.utils.state import State
from airflow.models.dagrun import DagRun
from datetime import datetime
from google.cloud import storage
import os

controller_version = '$2'
customer_version = '$1'
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
global_prefix_name = 'Continual_global_variable'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
global_params = Variable.get(global_prefix_name, deserialize_json=True)


# get task name from config file according to the phase
def get_task_name(phase):
    values = dict(get_phase_values(phase))
    print("task to run: {} ".format(values['execution_task']))
    return values['execution_task']


def begin_tasks():
    now = datetime.now()
    print("Controller start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Controller version: " + controller_version + ", Customer version: " + customer_version)


def time_shift(working_day, working_hour):
    working_day = datetime.strptime(working_day + '_' + working_hour, "%Y-%m-%d_%H:%M")
    working_exec_time = working_day + timedelta(hours=2)
    new_working_hours = datetime.strftime(working_exec_time, "%H:%M")
    return str(new_working_hours)


# trigger external dag according to a set of rulse
# 1 if the configure date time in the json file not after the current time.take into consideration the next day calculation
# 2 there are no external dag running
def run_daily_check(context, dag_run_obj):
    now = datetime.now()
    if (check_running_dags()):
        print("external DAG not finished executed, NOT triggering next task ")
    else:
        print("no tasks running, checking next phase and time ")
        phase = int(Variable.get(param_prefix_name + '_' + "phase"))

        values = dict(get_phase_values(phase))
        conf_execution_time = str(values['conf_execution_time'])
        day_shift = int(values['day_shift'])
        next_phase = int(values['next_phase'])
        working_day = datetime.strptime(
            str(Variable.get(param_prefix_name + '_' + "working_day") + '_' + conf_execution_time), "%Y-%m-%d_%H:%M")
        working_exec_time = working_day + timedelta(days=day_shift) + timedelta(
            minutes=int(Variable.get(param_prefix_name + '_' + "delta")))
        working_time = str(values['job_time'])
        print("phase to execute: {} ".format(phase))

        print("execution time calculation to compare for current time is (working_exec_time): {} ".format(
            datetime.strftime(working_exec_time, "%Y-%m-%d_%H:%M")))
        if (now >= working_exec_time):
            print("start phase no {} ".format(phase))
            Variable.set(param_prefix_name + '_' + "prev_phase", phase)
            Variable.set(param_prefix_name + '_' + "phase", next_phase)

            Variable.set(param_prefix_name + '_' + "working_hour_internal", working_time)
            working_hour_shift = time_shift(str(Variable.get(param_prefix_name + '_' + "working_day")), working_time)
            Variable.set(param_prefix_name + '_' + "working_hour_shift_internal", working_hour_shift)
            Variable.set(param_prefix_name + '_' + "working_date_internal", str(Variable.get(param_prefix_name + '_' + "working_day")))
            exec_time = str(Variable.get(param_prefix_name + '_' + "working_day") + '_' + working_time)

            print("allow to run,data parameters to send are: (date_hour), exec_time: {} ".format(exec_time))

            # dag_run_obj.payload = {'job_date': str(Variable.get(param_prefix_name + '_' + "working_day")),'job_time': working_time}

            if (int(Variable.get(param_prefix_name + '_' + "prev_phase")) > int(
                    Variable.get(param_prefix_name + '_' + "phase"))):
                print("starting new day ")
                next_working_day = datetime.strptime(str(Variable.get(param_prefix_name + '_' + "working_day")),
                                                     "%Y-%m-%d") + timedelta(days=1)
                Variable.set(param_prefix_name + '_' + "working_day", datetime.strftime(next_working_day, "%Y-%m-%d"))
            Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", True)
            return dag_run_obj
        else:
            print("phase and time not match, NOT allow to run ")


# get phase configuration for running
def get_phase_values(phase):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(str(general_params['composer_bucket_name']))
    blob = bucket.get_blob(str(general_params['flow_configuration_file']))
    flie_content = blob.download_as_string(client=None)
    json_s = json.loads(flie_content)
    for s in json_s['steps']:
        if (phase == int(s['phase_no'])):
            val = dict(s['values'])
            return val


# check if there are running dags according to the current phase or the previous phase task names
def check_running_dags():
    current_dagruns = DagRun.find(
        dag_id=get_task_name(int(Variable.get(param_prefix_name + '_' + "phase", "0"))),
        state=State.RUNNING
    )
    prev_dagruns = DagRun.find(
        dag_id=get_task_name(int(Variable.get(param_prefix_name + '_' + "prev_phase",
                                              int(Variable.get(param_prefix_name + '_' + "phase", "0"))))),
        state=State.RUNNING
    )
    if (len(current_dagruns) >= 1 or len(prev_dagruns) >= 1):
        return True
    else:
        if (Variable.get(param_prefix_name + '_' + "is_triggered_dag_running", False)):
            print(
                "Error: according to parameter: is_triggered_dag_running a trigger dag should run but not found any running dags according to logic, might be an error in external task")
        return False


default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    'wait_for_downstream': True,
    'depends_on_past': True,
    'owner': 'Airflow',
    'catchup': False,
    'end_date': datetime.strptime(str(Variable.get(param_prefix_name + '_' + "end_date")), "%Y-%m-%d"),
    'project': global_params["gcp_project"]
}
# global variable: end_date_var = 2020-07-12:04
# global variable: working_day = 2020-07-12
# global: flow_configuration_file = /usr/local/lib/python3.6/site-packages/airflow/example_dags/configuration.json
# global: is_triggered_dag_running - boolean, should be set to false in every last step of external triggered dag, default value = False (for first run)
# global variable: phase - int value, allowed values 0-6 (according to the json file configuration)
# global variable: delta time in minutes to shift the time execution parameters
with DAG(param_prefix_name,
         start_date=datetime.now() - timedelta(days=1),
         schedule_interval='0/10 * * * *',
         #         schedule_interval=None,
         default_args=default_dag_args
         ) as controller_dag:
    end_controller = DummyOperator(
        task_id="end_controller_1",
        trigger_rule='all_done')

    start_controller = PythonOperator(
        task_id='start_controller_1',
        provide_context=False,
        python_callable=begin_tasks
    )

    trigger_dag = TriggerDagRunOperator(
        task_id='trigger_dag',
        trigger_dag_id=get_task_name(int(Variable.get(param_prefix_name + '_' + "phase"))),
        python_callable=run_daily_check)

    start_controller >> trigger_dag >> end_controller