import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from subdag.dataproc_4h_subdag_VFG import dag_process
from airflow.operators.subdag_operator import SubDagOperator
from airflow.utils.dates import days_ago
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
spark_properties = 'spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8'

DATAPROC_SPARK_PROP = {
    'spark.executor.instances': '3',
    'spark.executor.memoryOverhead': '4g',
    'spark.executor.cores': '3',
    'spark.executor.memory': '10g',
    'spark.master': 'yarn'
}  # Dict mentioning Spark job's properties


def end_task_clean():
    # set finish Dag execution
    Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    now = datetime.now()
    print("Finish run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    # update dad execution stratus, if one task failed mark the dag as failed
    # current_dag=get_dag()
    # current_dag.update_state()
    

def intermediate_step():
    print("step complete")



def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


default_dag_args = {
    'email_on_failure': True,
    'email_on_retry': False,
    'email_on_success': True,
    'email': ['vadim.zinchuk@continualexperience.com', 'ori.orenbach@continualexperience.com'],
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

def create_dataproc_cluster(name):
    return BashOperator(
    task_id='create_dataproc_cluster_' + name,
    bash_command="""
        gcloud beta dataproc clusters create """ + global_cluster_name + '-' + name.replace("_","-") + """ \
        --bucket vf-de-nwp-live-prodb \
        --region europe-west3 \
        --zone """ + str(global_params['gcp_zone']) + """ \
        --tags allow-internal-dataproc-prodb,allow-ssh-from-management-zone-prodb \
        --project """ + str(global_params['gcp_project']) + """ \
        --subnet projects/vf-de-nwp-live/regions/europe-west3/subnetworks/prodb-restricted-zone \
        --service-account vf-de-nwp-prodb-dp-ops-sa@vf-de-nwp-live.iam.gserviceaccount.com \
        --metadata enable-oslogin=true \
        --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
        --enable-component-gateway \
        --no-address \
        --master-machine-type n1-standard-16 \
        --master-boot-disk-size 400 \
        --worker-machine-type n1-standard-32 \
        --worker-boot-disk-size 100 \
        --num-workers 20 \
        --max-idle 1h \
        --image-version 1.3-debian10 \
        """
    )
    
def delete_dataproc_cluster(name):
    return BashOperator(
        task_id='delete_dataproc_cluster_' + name,
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + '-' + name.replace("_","-") + """ \
            --region europe-west3 \
        """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )

with DAG(
        "dataproc_4hours_cycle_VFG",
        start_date=days_ago(1),
        schedule_interval=None,
        # concurrency=64,
        # catchup=False,
        default_args=default_dag_args)as dag:

    final_step = PythonOperator(
        task_id='final_step',
        provide_context=False,
        python_callable=end_task_clean,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )
    
    # intermediate_step_1 = PythonOperator(
        # task_id='intermediate_step_1',
        # provide_context=False,
        # python_callable=intermediate_step,
        # trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    # )
    
    # intermediate_step_2 = PythonOperator(
        # task_id='intermediate_step_2',
        # provide_context=False,
        # python_callable=intermediate_step,
        # trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    # )

    # first_step = PythonOperator(
        # task_id='first_step',
        # provide_context=False,
        # python_callable=begin_tasks
    # )
    
    region = str(general_params['customer'])
    region_list = region.split(",")
    # create_clusters_list = []
    # delete_clusters_list = []
    # for customer in region_list:
        # create_task = create_dataproc_cluster(customer)
        # create_clusters_list.append(create_task)
        # delete_task = delete_dataproc_cluster(customer)
        # delete_clusters_list.append(delete_task)

    subdag_list = []
    for customer in region_list:
        args2 = {
            'customer_name': customer
        }
        dynamic = SubDagOperator(
            task_id=customer,
            subdag=dag_process('dataproc_4hours_cycle_VFG', customer, args2)
        )
        
        create_task = create_dataproc_cluster(customer)
        delete_task = delete_dataproc_cluster(customer)
        subdag_list.append(create_task >> dynamic >> delete_task)

    subdag_list >> final_step

