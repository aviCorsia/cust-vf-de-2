import pandas as pd
from datetime import datetime,date,timedelta
import logging
import csv
import numpy as np
import glob
import subprocess as sp
import configparser 
import argparse
import os
import glob
from pathlib import Path

# check if config.ini file exists
if not os.path.exists('/home/gis/config.ini'):
    print('Config file not found.')
    exit()

# read the config.ini file
config = configparser.RawConfigParser()
config.read('/home/gis/config.ini')

gis_region_list = str(config.get('GIS', 'region_list'))
last_gis_file = str(config.get('GIS', 'last_gis_file'))
autoGIS_dir = str(config.get('GIS', 'autoGIS_dir'))


home_dir = '/home/gis/'
gis_dir = home_dir + "gis/"
log_dir = home_dir + "log/"
gis_raw_dir = home_dir + "gis_raw/"
fcn_GSM_file = home_dir + 'fcn_map_GSM.csv'
fcn_UMTS_file = home_dir + 'fcn_map_UMTS.csv'
fcn_LTE_file = home_dir + 'fcn_map_LTE.csv'

now = datetime.now()
yesterday = date.today() - timedelta(days=1)
date_time = now.strftime("%Y%m%d%H%M%S")

gisheader = ['Name', 'CILAC', 'SectorID', 'RNC_BSC', 'LAC', 'SectorType', 'Scr_Freq', 'uarfcn', 'BSIC', 'tech',
             'latitude', 'longitude', 'Bearing', 'AvgNeighborDist', 'MaxNeighborDist', 'NeighborsCount', 'Eng',
             'TiltE', 'TiltM', 'SiteID', 'AdminCellState', 'Asset', 'Asset_Configuration', 'Cell_Type', 'cell_name',
             'city', 'Height', 'RF_Team', 'Asset_Calc', 'Sector_uniq', 'FreqType', 'TAC', 'RAC', 'Band', 'Vendor',
             'CPICHPwr', 'MaxTransPwr', 'FreqMHz', 'HBW', 'VBW', 'SITE_NAME','MCC', 'MNC']

gisheader2 = 'Name,CILAC,SectorID,RNC_BSC,LAC,SectorType,Scr_Freq,uarfcn,BSIC,tech,latitude,longitude,Bearing,AvgNeighborDist,MaxNeighborDist,NeighborsCount,Eng,TiltE,TiltM,SiteID,AdminCellState,Asset,Asset_Configuration,Cell_Type,cell_name,city,Height,RF_Team,Asset_Calc,Sector_uniq,FreqType,TAC,RAC,Band,Vendor,CPICHPwr,MaxTransPwr,FreqMHz,HBW,VBW,SITE_NAME,MCC,MNC,RSI,PCI,cell_range,dlbandwidth,district,postal_code,sharing_flag,federal_state,so_code,so_name'

lof_f = log_dir + "outputGIS_" + date_time + '.log'  
gis_file = gis_dir + 'gis_' + date_time + '_temp.csv' 
gis_file2 = gis_dir + 'gis_' + date_time + '.csv'  
gis_region_east = gis_dir + 'gis_east_' + date_time + '.csv' 
gis_region_west = gis_dir + 'gis_west_' + date_time + '.csv' 



def create_gis(file):
    logging.info('create GIS file.')
    dfOutputGis = pd.DataFrame(columns=gisheader)
    dfg = pd.read_csv(file, low_memory=False, dtype=str,encoding='latin_1')

    dfg = dfg[dfg['latitude'].notna()].reset_index() #remove latitude with text "nan"
    dfg = dfg[dfg['frequency'].notna()].reset_index() #remove frequency with text "nan"

    dfg['cell_type'] = np.where(dfg['indoor_flag'].map(str) == '0', 'OUTDOOR',
                                np.where(dfg['indoor_flag'].map(str) == '1', 'INDOOR',
                                    np.where(((dfg['antenna_h_beamwidth'].map(str) == '360') & (dfg['indoor_flag'].map(str) == '0')), 'OMNI','OUTDOOR')))
    dfg['enodeb_id'] = dfg['enodeb_id'].fillna(0)
    dfg['cgi_x'] = np.where(dfg['rat'].map(str) == "LTE",(dfg['enodeb_id'].map(int)*256)+dfg['cell_id'].map(int),0)   
    dfg['UARFCN'] = np.where(dfg['rat'].map(str) == "LTE", dfg['earfcndl'].map(str),np.where(dfg['rat'].map(str) == "GSM", '', dfg['uarfcn'].map(str)))           
    #dfg['so_name'] = dfg['so_name'].map(str).replace(',','', regex=True)
    #dfg.to_csv(home_dir + "check.csv", mode='w', header=True, index=False,encoding='utf_8')

    ### --- fix the vendor that are null
    dfg['vendor_new_x'] = np.where(dfg['vendor'].map(str).replace('nan','') == '', dfg['cell_name'].str.slice(stop=1),dfg['vendor'].map(str))
    dfg['vendor_new'] = np.where(dfg['vendor_new_x'].isin(['D', 'F', 'S', 'W']), 'Ericsson',
                                np.where(dfg['vendor_new_x'].isin(['B', 'H', 'M', 'O']), 'Huawei',dfg['vendor'].map(str)))

    dfg['vendor'] = np.where(dfg['vendor'].map(str).replace('nan','') == '', dfg['vendor_new'].map(str), dfg['vendor'].map(str))
    dfg = dfg.drop('vendor_new_x',axis=1)
    dfg = dfg.drop('vendor_new',axis=1)
    ### -----------

    dfOutputGis['Name'] = dfg['cell_name']
    dfOutputGis['CILAC'] = np.where(dfg['rat'].map(str) == "LTE", '3' + dfg['cgi_x'].map(str),
                                    np.where(dfg['rat'].map(str) == "GSM", '1' + dfg['lac'].map(str) + dfg['cell_id'].map(str),
                                                '2' + dfg['lac'].map(str) + dfg['cell_id'].map(str)))
    dfOutputGis['SectorID'] = np.where(dfg['rat'].map(str) == "LTE" ,dfg['cgi_x'].map(str), dfg['cell_id'].map(str))
    #dfOutputGis['RNC_BSC'] ####
    ##dfOutputGis['LAC'] = np.where(dfg['rat'].map(str) == "LTE", dfg['tac'].map(str),dfg['lac'].map(str)) 
    dfOutputGis['LAC'] = np.where(dfg['rat'].map(str) == "LTE", '-1',dfg['lac'].map(str)) 
    #dfOutputGis['SectorType'] ####
    #dfOutputGis['Scr_Freq'] ####
    dfOutputGis['uarfcn'] = dfg['UARFCN'].map(str).replace('nan', '')
    dfOutputGis['BSIC'] = '-1'
    dfOutputGis['tech'] = dfg['rat'].map(str) + '_' + dfg['vendor'].map(str)
    dfOutputGis['latitude'] = dfg['latitude'].map(str)
    dfOutputGis['longitude'] = dfg['longitude'].map(str)
    dfOutputGis['Bearing'] = dfg['azimuth'].map(str).replace('nan','')
    #dfOutputGis['AvgNeighborDist'] ####
    #dfOutputGis['MaxNeighborDist'] ####
    #dfOutputGis['NeighborsCount'] ####
    dfOutputGis['Eng'] = dfg['cell_name'].map(str).astype(str).str[0]
    dfOutputGis['TiltE'] = dfg['electrical_tilt'].map(str).replace('nan','')
    dfOutputGis['TiltM'] = dfg['mechanical_tilt'].map(str).replace('nan','')
    dfOutputGis['SiteID'] = dfg['site_id'].map(str).replace('nan','')
    #dfOutputGis['AdminCellState'] ####
    #dfOutputGis['Asset']
    #dfOutputGis['Asset_Configuration'] ####
    dfOutputGis['Cell_Type'] = dfg['cell_type'].map(str).replace('nan','OUTDOOR')
    #dfOutputGis['cell_name'] ####
    dfOutputGis['City'] = dfg['city'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['Height'] = dfg['height'].str.split('.').str[0]
    dfOutputGis['RF_Team'] = dfg['region'].map(str).replace('nan','')
    #dfOutputGis['Asset_Calc'] 
    #dfOutputGis['Sector_uniq'] ####
    #dfOutputGis['FreqType'] ####
    dfOutputGis['TAC'] = np.where(dfg['rat'].map(str) == 'LTE', dfg['tac'].map(str), '')
    #dfOutputGis['RAC'] ####
    dfOutputGis['Band'] = dfg['frequency'].map(str).replace('nan','')
    dfOutputGis['Vendor'] = dfg['vendor'].map(str).replace('nan','')
    #dfOutputGis['CPICHPwr'] ####
    dfOutputGis['MaxTransPwr'] = np.where(dfg['rat'].map(str) == 'GSM', '', dfg['maxtxpower'].map(str))
    #dfOutputGis['FreqMHz'] ####
    dfOutputGis['HBW'] = dfg['antenna_h_beamwidth'].map(str).replace('nan','')
    dfOutputGis['VBW'] = dfg['antenna_v_beamwidth'].map(str).replace('nan','')
    dfOutputGis['SITE_NAME'] = dfg['site_name'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['MCC'] = dfg['mcc'].map(str).replace('nan','')
    dfOutputGis['MNC'] = dfg['mnc'].map(str).replace('nan','')
    dfOutputGis['RSI'] = dfg['rachrootsequence'].map(str).replace('nan','')
    dfOutputGis['PCI'] = dfg['pci'].map(str).replace('nan','')
    dfOutputGis['cell_range'] = dfg['cell_range'].map(str).replace('nan','')
    dfOutputGis['dlbandwidth'] = dfg['dlbandwidth'].map(str).replace('nan','')
    dfOutputGis['district'] = dfg['district'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['postal_code'] = dfg['postal_code'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['sharing_flag'] = dfg['sharing_flag'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['federal_state'] = dfg['federal_state'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['so_code'] = dfg['so_code'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['so_name'] = dfg['so_name'].map(str).replace('nan','').replace(',','', regex=True)
    dfOutputGis['FBAND'] = dfg['network_layer'].map(str).replace(',','', regex=True)

    # calc asset number
    nf = pd.DataFrame(columns=['A', 'B'])
    # A
    nf['A'] = (dfg['latitude'].map(float) * 1000).map(int) - int(dfg['latitude'].map(float).min()) * 1000
    # B
    nf['B'] = (dfg['longitude'].map(float) * 1000).map(int) - int(dfg['longitude'].map(float).min()) * 1000
    dfOutputGis['Asset'] = np.where(nf['A'] >= nf['B'], nf['A'] * (nf['A'] + 1) + nf['B'], nf['A'] + nf['B'] * nf['B'])
    dfOutputGis['Asset_Calc'] = np.where(nf['A'] >= nf['B'], nf['A'] * (nf['A'] + 1) + nf['B'], nf['A'] + nf['B'] * nf['B'])
    dfOutputGis['Sector_uniq'] = dfOutputGis['Asset_Calc'].map(str) + '#' + dfOutputGis['Bearing'].map(str)

    ############## update the FreqType and SectorType ####################
    dfOutputGis['tech2'] = np.where(dfOutputGis['tech'].str[:3] == 'UMT', 'UMTS', np.where(dfOutputGis['tech'].str[:3] == 'NR_', 'NR', dfOutputGis['tech'].str[:3]))
    dfOutputGis['Band'] = dfOutputGis['Band'].map(int)
    
    sectortypedf = dfOutputGis[['tech2','Band','uarfcn']]
    sectortypedf = sectortypedf[~sectortypedf.tech2.eq('GSM')]
    sectortypedf['count'] = 0
    sectortypedf_freqtype = sectortypedf.groupby(['tech2','Band','uarfcn'])['count'].count().reset_index(name='count')
    sectortypedf_freqtype['rank1'] = sectortypedf_freqtype.groupby(['tech2','Band'])['count'].rank(method="first", ascending=False)
    sectortypedf_freqtype['FreqType_new'] = 'F'+sectortypedf_freqtype['rank1'].map(int).map(str)

    sectortypedf_freqtype = sectortypedf_freqtype.sort_values(by=['tech2', 'Band', 'FreqType_new'])

    
    
    sectortypedf_freqtype['sectortype'] = sectortypedf_freqtype.groupby(['tech2'])['Band'].rank(method="first", ascending=True)
    sectortypedf_freqtype['sectortype'] = sectortypedf_freqtype['sectortype'] +\
        np.where(sectortypedf_freqtype['tech2']=='UMTS', 7, 0) +\
        np.where(sectortypedf_freqtype['tech2']=='LTE', 15, 0) +\
        np.where(sectortypedf_freqtype['tech2']=='NR', 30, 0)
    
    sectortypedf_freqtype = sectortypedf_freqtype[['tech2', 'uarfcn', 'Band', 'FreqType_new','sectortype']]

    df_new = pd.merge(dfOutputGis, sectortypedf_freqtype, how='left', on=['tech2', 'uarfcn','Band'])
    #print(sectortypedf_freqtype)
    df_new['sectortype'] = np.where((df_new['tech'].str[:3] == 'GSM') & (df_new['Band'].map(str) == '1800'),'1', np.where((df_new['tech'].str[:3] == 'GSM') & (df_new['Band'].map(str) == '900'),'2', df_new['sectortype']))
   
    #print(df_new.index)
    #print(dfOutputGis.index)
    dfOutputGis['FreqType'] = df_new['FreqType_new']
    dfOutputGis['SectorType'] = df_new['sectortype'].str.split('.').str[0]
    dfOutputGis = dfOutputGis.drop('tech2', 1)
    dfOutputGis = dfOutputGis.drop('FBAND', 1)
    ######################################################################
    #print(dfOutputGis.district.unique())


    # create the GIS file
    logging.info('create GIS file: ' + gis_file)
    #dfOutputGis.to_csv(gis_file, mode='w', header=False, index=False)
    dfOutputGis.to_csv(gis_file, mode='w', header=True, index=False,encoding='utf_8')
    
    # ---> update FreqMhz column
    with open(gis_file2, 'w', newline='') as f2:
        f2.write(gisheader2+'\n')
        with open(gis_file, 'r') as file:
            csv_file = csv.DictReader(file)
            for row in csv_file:
                ### UMTS
                if (row['uarfcn'] == '') and ('UMTS' in str(row['tech'])):
                    row['FreqMHz'] = '1999'
                    pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                    f2.write(pat+'\n')
                elif (row['uarfcn'] != '') and ('UMTS' in str(row['tech'])):
                    with open(fcn_UMTS_file, 'r') as fileg:
                        csv_fileg = csv.DictReader(fileg)
                        for rowu in csv_fileg:
                            if (int(row['uarfcn']) >= int(rowu['code_low'])) and (int(row['uarfcn']) <= int(rowu['code_high'])):
                                row['FreqMHz'] = str(rowu['dl_middle_MHz'])
                                pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                                f2.write(pat+'\n')
                    if row['FreqMHz'] == '':
                        row['FreqMHz'] = '1999'
                        pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                        f2.write(pat+'\n')                 
                ### GSM
                elif (row['uarfcn'] == '') and ('GSM' in str(row['tech'])):
                    row['FreqMHz'] = '1999'
                    pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                    f2.write(pat+'\n')
                elif (row['uarfcn'] != '') and ('GSM' in str(row['tech'])):
                    with open(fcn_GSM_file, 'r') as fileg:
                        csv_fileu = csv.DictReader(fileg)
                        for rowl in csv_fileu:
                            if (int(row['uarfcn']) >= int(rowl['code_low'])) and (int(row['uarfcn']) <= int(rowl['code_high'])):
                                row['FreqMHz'] = str(rowl['dl_middle_MHz'])
                                pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                                f2.write(pat+'\n')
                    if row['FreqMHz'] == '':
                        row['FreqMHz'] = '1999'
                        pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                        f2.write(pat+'\n')                 
                
                ### LTE
                elif (row['uarfcn'] == '') and ('LTE' in str(row['tech'])):
                    row['FreqMHz'] = '1999'
                    pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                    f2.write(pat+'\n')
                elif (row['uarfcn'] != '') and ('LTE' in str(row['tech'])):
                    with open(fcn_LTE_file, 'r') as fileg:
                        csv_filel = csv.DictReader(fileg)
                        for rowl in csv_filel:
                            if (int(row['uarfcn']) >= int(rowl['code_low'])) and (int(row['uarfcn']) <= int(rowl['code_high'])):
                                row['FreqMHz'] = str(rowl['dl_middle_MHz'])
                                pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                                f2.write(pat+'\n') 
                        
                    if row['FreqMHz'] == '':
                        row['FreqMHz'] = '1999'
                        pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                        f2.write(pat+'\n')    
    

    # ---> split the GIS to regions
    for l in gis_region_list.split(','):
        for r in l.split('-'):
            file_name = r
            with open(gis_dir + file_name + '_' + date_time +'.csv', 'w', newline='') as f2:
                f2.write(gisheader2+'\n')
                for r in l.split('-'):
                    if r != file_name:
                        with open(gis_file2, 'r') as file:
                            csv_file = csv.DictReader(file)
                            for row in csv_file:
                                if (row['RF_Team'] == r):
                                    pat = row['Name']+','+row['CILAC']+','+row['SectorID']+','+row['RNC_BSC']+','+row['LAC']+','+row['SectorType']+','+row['Scr_Freq']+','+row['uarfcn']+','+row['BSIC']+','+row['tech']+','+row['latitude']+','+row['longitude']+','+row['Bearing']+','+row['AvgNeighborDist']+','+row['MaxNeighborDist']+','+row['NeighborsCount']+','+row['Eng']+','+row['TiltE']+','+row['TiltM']+','+row['SiteID']+','+row['AdminCellState']+','+row['Asset']+','+row['Asset_Configuration']+','+row['Cell_Type']+','+row['cell_name']+','+row['city']+','+row['Height']+','+row['RF_Team']+','+row['Asset_Calc']+','+row['Sector_uniq']+','+row['FreqType']+','+row['TAC']+','+row['RAC']+','+row['Band']+','+row['Vendor']+','+row['CPICHPwr']+','+row['MaxTransPwr']+','+row['FreqMHz']+','+row['HBW']+','+row['VBW']+','+row['SITE_NAME']+','+row['MCC']+','+row['MNC']+','+row['RSI']+','+row['PCI']+','+row['cell_range']+','+row['dlbandwidth']+','+row['district']+','+row['postal_code']+','+row['sharing_flag']+','+row['federal_state']+','+row['so_code']+','+row['so_name']
                                    f2.write(pat+'\n') 
            break    


def upload_gis_to_bucket():
    config = configparser.ConfigParser()
    config.read('/home/gis/config.ini')
    for section in config.sections():
        if section == 'GIS_BUCKET':
            for key in config[section]:
                r_name = key.replace('_dir_in_bucket','')
                for file_name in glob.glob('/home/gis/gis/*.csv'):
                    if r_name in file_name:
                        r_dir = str(config[section][key])
                        output = sp.getoutput('gsutil -m cp ' + file_name + ' gs://' + r_dir + '/gis_new.txt')


def update_config_last_gis_date(date_check):
    config = configparser.RawConfigParser()
    config.read('/home/gis/config.ini')
    config.set('GIS', 'last_gis_file', date_check)                         
    cfgfile = open('/home/gis/config.ini','w')
    config.write(cfgfile, space_around_delimiters=False)  # use flag in case case you need to avoid white space.
    cfgfile.close()

    # upload the update config.ini file to bucket
    output = sp.getoutput('gsutil cp /home/gis/config.ini gs://' + autoGIS_dir + '/autoGIS')


def check_last_gis_date(date_check):
    if last_gis_file == date_check:
        print('already work on this date: ' + date_check)
        exit()

def main():
    ##  ______ MAIN _________
    # Start Log file
    logging.basicConfig(filename=lof_f, format='%(asctime)s %(message)s', level=logging.INFO)
    logging.info('Started AutoGIS ...')

    # check if the folder is empty
    if not os.listdir(gis_raw_dir):
        print('No GIS file to work on.')
    else:
        for file in glob.glob(gis_raw_dir+'*.csv'):
            logging.info('working on file: ' + file)

            date_check = str(Path(file).stem).replace('export_gis_','')

            check_last_gis_date(date_check)
            create_gis(file)
        
        upload_gis_to_bucket()
        update_config_last_gis_date(date_check)

    logging.info('Done. script finised')
    ##  ______ END MAIN _________


if __name__ == "__main__":
    main()
