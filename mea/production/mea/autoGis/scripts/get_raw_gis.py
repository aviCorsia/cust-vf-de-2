import pyspark 
import sys
import pandas as pd
from pyspark.sql import SparkSession
from datetime import datetime, timedelta
import subprocess as sp
from pathlib import Path
import dateutil.relativedelta
import configparser 
import argparse
import os


runOnDay = sys.argv[1]

Path('/home/gis/').mkdir(parents=True, exist_ok=True)
Path('/home/gis/log/').mkdir(parents=True, exist_ok=True)
Path('/home/gis/gis_raw/').mkdir(parents=True, exist_ok=True)
Path('/home/gis/gis/').mkdir(parents=True, exist_ok=True)


# check if config.ini file exists
if not os.path.exists('/home/gis/config.ini'):
    print('Config file not found.')
    exit()

config = configparser.RawConfigParser()
config.read('/home/gis/config.ini')

raw_gis_dir = str(config.get('GIS', 'raw_gis_dir')) 
autoGIS_dir = str(config.get('GIS', 'autoGIS_dir'))

# Decomoses the date to day, month, year
getdate = datetime.strptime(runOnDay,'%Y-%m-%d')
v_year = getdate.strftime('%Y')
v_month = getdate.strftime('%m')
v_day = getdate.strftime('%d')

# get the max day in the month
output = sp.getoutput("gsutil ls gs://" + raw_gis_dir + "/year="+str(v_year)+"/month="+str(v_month).replace('0','')+"/ |  awk -v FS='/' '{print $10}'")
newlist = output.replace('0','').replace('day=','')
arry = []
for i in newlist.split("\n"):
    print(int(i))
    arry.append(int(i))

if not arry:
    # the list is empty, then exit
    print("No files for month: " + v_month+'/'+v_year)
    exit()

print("the month: " + str(v_month) + " days: "+ str(arry))
print("max day: " + str(int(max(arry))))

max_day = str(max(arry))

spark = SparkSession.builder.master("yarn")\
.appName("demo")\
.config("spark.sql.broadcastTimeout", "36000")\
.config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
.getOrCreate()

df = spark.read.parquet('gs://' + raw_gis_dir + '/year='+str(v_year)+'/month='+str(v_month).replace('0','')+'/day='+str(max_day)+'/*.parquet')
df.coalesce(1).write.mode('overwrite').option("mapreduce.fileoutputcommitter.marksuccessfuljobs","false").option("header","true").csv("gs://" + autoGIS_dir + "/autoGIS/gis_raw/"+str(v_year)+str(v_month)+str(max_day))

# download the raw gis file to the cluster
output = sp.getoutput('gsutil -m cp gs://' + autoGIS_dir + '/autoGIS/gis_raw/'+str(v_year)+str(v_month)+str(max_day)+'/*.csv /home/gis/gis_raw/export_gis_'+str(v_year)+str(v_month)+str(max_day)+'.csv')

# download the run_sed.sh script file to the cluster
output = sp.getoutput('gsutil -m cp gs://' + autoGIS_dir + '/autoGIS/scripts/run_sed.sh /home/gis/run_sed.sh')
output = sp.getoutput('chmod +x /home/gis/run_sed.sh')
output = sp.getoutput('/home/gis/run_sed.sh ' + '/home/gis/gis_raw/export_gis_'+str(v_year)+str(v_month)+str(max_day)+'.csv')


# download the tech files for the gis process
output = sp.getoutput('gsutil -m cp gs://' + autoGIS_dir + '/autoGIS/tech_files/fcn_map_GSM.csv /home/gis/fcn_map_GSM.csv')
output = sp.getoutput('gsutil -m cp gs://' + autoGIS_dir + '/autoGIS/tech_files/fcn_map_UMTS.csv /home/gis/fcn_map_UMTS.csv')
output = sp.getoutput('gsutil -m cp gs://' + autoGIS_dir + '/autoGIS/tech_files/fcn_map_LTE.csv /home/gis/fcn_map_LTE.csv')



