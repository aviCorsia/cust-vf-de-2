import datetime
import os
import uuid

from airflow import DAG
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

global_cluster_name= 'processing-1-full-day'

general_params={
    "ENRICHMENT_JAR": "gs://vf-continual-nwp-nonlive-qa/jars/processing-enrichment-5.5.RC1-jar-with-dependencies.jar",
    "MOBILITY_JAR" : "gs://vf-continual-nwp-nonlive-qa/jars/processing-mobility-5.5.RC1-jar-with-dependencies.jar",
    "AGGS_JAR" : "gs://vf-continual-nwp-nonlive-qa/jars/processing-aggs-5.5.RC1-jar-with-dependencies.jar",
    "ENRICHMENT_MAIN_CLASS" : "com.continual.sonata.Processing.Enrichment.Host.Host",
    "MOBILITY_MAIN_CLASS" : "com.continual.sonata.Mobility.Processing.Host.Host",
    "AGGS_MAIN_CLASS" : "com.continual.sonata.processing.host.Host",
    "MOBILITY_PRE_PROCESS_MAIN_CLASS" : "com.continual.sonata.Mobility.PreProcessing.Host.Host",
    "CLUSTER_NAME" : "continual-mea-cluster-{{ ds_nodash }}",
    "customer" : "nord",
    "home_folder" : "gs://vf-continual-nwp-nonlive-qa//",
    "auto_delete_duration": 14400,
    "flow_configuration_file" : "data/configuration.json",
    "composer_bucket_name" : "europe-west3-mea-composer-47449e81-bucket",
    "working_day":"2021-06-06"
}

default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': "vf-continual-nwp-nonlive"
}

with DAG(
        "pre_processing_transitions_prepare_VFG",
        start_date=datetime.now() - timedelta(days=1),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
    
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters create """ + global_cluster_name + """ \
            --bucket vf-continual-nwp-nonlive-qa \
            --region europe-west3 \
            --zone europe-west3-b \
            --tags allow-internal-dataproc-dev,allow-ssh-from-management-zone,allow-ssh-from-net-to-bastion \
            --project vf-continual-nwp-nonlive \
            --subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/dev-restricted-zone \
            --service-account vf-cont-nwp-dev-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
            --metadata enable-oslogin=true \
            --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
            --optional-components=ANACONDA,JUPYTER,ZEPPELIN \
            --enable-component-gateway \
            --no-address \
            --master-machine-type n1-standard-8 \
            --worker-machine-type n1-standard-32 \
            --num-workers 8 \
            --max-idle 1h 
        """
    )
    pre_processing_transitions = BashOperator(
        task_id='pre_processing_transitions',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
            --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
            --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8 \
            -- customer="""+ str(general_params['customer']) + """ \
             flow=map_prepare \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=01:00 \
             job_date=""" + str(general_params['working_day']) + """ \
             type=pre_processing_transitions \
             gcp=true 
        """
    )
    delete_dataproc_cluster = BashOperator(
        task_id='delete_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + """ \
            --region europe-west3 \
        """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )
    create_dataproc_cluster >> pre_processing_transitions >> delete_dataproc_cluster