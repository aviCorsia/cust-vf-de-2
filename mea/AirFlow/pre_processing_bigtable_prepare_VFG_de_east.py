import datetime
import os
import uuid

from airflow import DAG
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

#name_convention  Continual_<customer>_<controller_dag_id>
global_cluster_name= 'processing-1-full-day-2'

general_params={
    "ENRICHMENT_JAR": "gs://vf-continual-nwp-nonlive-qa/jars/5.6/processing-enrichment-5.6-RC8-jar-with-dependencies.jar", 
    "MOBILITY_JAR" : "gs://vf-continual-nwp-nonlive-qa/jars/5.6/processing-mobility-5.6-RC8-gcp-jar-with-dependencies.jar", 
    "AGGS_JAR" : "gs://vf-continual-nwp-nonlive-qa/jars/5.6/processing-aggs-5.6-RC8-jar-with-dependencies.jar", 
    "ENRICHMENT_MAIN_CLASS" : "com.continual.sonata.Processing.Enrichment.Host.Host",
    "MOBILITY_MAIN_CLASS" : "com.continual.sonata.Mobility.Processing.Host.Host",
    "AGGS_MAIN_CLASS" : "com.continual.sonata.processing.host.Host",
    "MOBILITY_PRE_PROCESS_MAIN_CLASS" : "com.continual.sonata.Mobility.PreProcessing.Host.Host",
    "CLUSTER_NAME" : "continual-mea-cluster-{{ ds_nodash }}",
    "customer" : "de_east",
    "home_folder" : "gs://vf-continual-nwp-nonlive-qa//",
    "auto_delete_duration": 14400,
    "flow_configuration_file" : "data/configuration.json",
    "composer_bucket_name" : "europe-west3-mea-composer-47449e81-bucket",
    "working_day":"2021-07-20"
}

default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': "vf-continual-nwp-nonlive"
}

with DAG(
        "pre_processing_bigtable_prepare_VFG_de_east",
        start_date=datetime.now() - timedelta(days=1),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
    
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters create """ + global_cluster_name + """ \
            --bucket vf-continual-nwp-nonlive-qa \
            --region europe-west3 \
            --zone europe-west3-b \
            --tags allow-ssh-from-management-zone-qa,allow-internal-dataproc-qa,allow-ssh-from-net-to-bastion \
            --project vf-continual-nwp-nonlive \
            --subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/qa-restricted-zone \
            --service-account vf-cont-nwp-qa-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
            --metadata enable-oslogin=true \
            --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
            --enable-component-gateway \
            --no-address \
            --master-machine-type n1-standard-8 \
            --worker-machine-type n1-standard-32 \
            --num-workers 16 \
            --max-idle 1h \
            --image-version 1.3-debian10 \
            """
    )
    pre_processing_map = BashOperator(
        task_id='pre_processing_map',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
            --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
            --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=16 \
            -- customer="""+ str(general_params['customer']) + """ \
             flow=map_prepare \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=00:00 \
             job_date=""" + str(general_params['working_day']) + """ \
             type=pre_processing_map \
             gcp=true 
        """
    )
    pre_cell_candidates = BashOperator(
        task_id='pre_cell_candidates',
        bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
                --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=16 \
                -- customer=""" + str(general_params['customer']) + """ \
                 flow=map_prepare \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=01:00 \
                 job_date=""" + str(general_params['working_day']) + """ \
                 type=pre_processing_cell_candidates \
                 gcp=true 
            """
    )
    pre_processing_transitions = BashOperator(
        task_id='pre_processing_transitions',
        bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                --class=""" + str(general_params['MOBILITY_PRE_PROCESS_MAIN_CLASS']) + """ \
                --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=16 \
                -- customer=""" + str(general_params['customer']) + """ \
                 flow=map_prepare \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=01:00 \
                 job_date=""" + str(general_params['working_day']) + """ \
                 type=pre_processing_transitions \
                 gcp=true 
            """
    )
    delete_dataproc_cluster = BashOperator(
        task_id='delete_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + """ \
            --region europe-west3 \
            """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )
    create_dataproc_cluster >> pre_processing_map >> pre_cell_candidates >> pre_processing_transitions>> delete_dataproc_cluster