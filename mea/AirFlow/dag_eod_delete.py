from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.models import Variable
from airflow.utils.dates import days_ago
import subprocess as sp
from google.cloud import storage

# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
src_bucket = general_params['home_folder'].replace("gs:", "").replace("//", "")
# eod variable
eod_params = Variable.get(param_prefix_name + '_eod', deserialize_json=True)
processing_day_path = eod_params['processing_day_path']
cdrs_path = eod_params['cdrs_path']
delete_folders_no_aggs_in = eod_params['delete_folders_not_aggs']


# read runtime date and 
#get cdrs days to keep
days_to_subtruct_cdrs = eod_params['days_to_keep_cdrs']
working_day_str = str(Variable.get(param_prefix_name + '_' + "working_day"))
date_time_str = datetime.strptime(working_day_str, "%Y-%m-%d") - timedelta(days=days_to_subtruct_cdrs)
day_delete_from_cdrs = date_time_str.strftime('%Y-%m-%d')

#get processing full days to keep
days_to_subtruct_processing_full = eod_params['days_to_keep_processing_full']
working_day_str = str(Variable.get(param_prefix_name + '_' + "working_day"))
date_time_str = datetime.strptime(working_day_str, "%Y-%m-%d") - timedelta(days=days_to_subtruct_processing_full)
day_delete_from_processing_full = date_time_str.strftime('%Y-%m-%d')

#get processing aggs folders only days to keep
days_to_subtruct_processing_aggs = eod_params['days_to_keep_processing_aggs']
working_day_str = day_delete_from_processing_full
date_time_str = datetime.strptime(working_day_str, "%Y-%m-%d") - timedelta(days=days_to_subtruct_processing_aggs)
day_delete_from_processing_aggs = date_time_str.strftime('%Y-%m-%d')


default_dag_args = {
    # Setting start date as yesterday starts the DAG immediately when it is
    # detected in the Cloud Storage bucket.
    'start_date': days_ago(1),
    # To email on failure or retry set 'email' arg to your email and enable
    # emailing here.
    'email_on_failure': False,
    'email_on_retry': False,
    # If a task fails, retry it once after waiting at least 5 minutes
    'retries': 0,
    'project_id': global_params['gcp_project']
}

def delete_processing_day_func(customer, vDateAggs, vDateFull):
    output = sp.getoutput("gsutil ls gs://" + src_bucket + "/" + processing_day_path + "/" + customer + " | awk -v FS='/' '{print $6}' ")
    for checkDay in output.split("\n"):
        if checkDay!="":
            if checkDay.replace("-","") < vDateAggs.replace("-",""):
                print("folder to delete: " + checkDay + " less then: " + vDateAggs)
                storage_client = storage.Client()
                bucket = storage_client.get_bucket(src_bucket)
                blobs = bucket.list_blobs(prefix=processing_day_path + '/' + customer + '/' + checkDay)
                for blob in blobs:
                    blob.delete()      

    # keep the agg folders
    output = sp.getoutput("gsutil ls gs://" + src_bucket + "/" + processing_day_path + "/" + customer + " | awk -v FS='/' '{print $6}' ")
    for checkDay in output.split("\n"):
        if checkDay!="":
            if checkDay.replace("-","") <= vDateFull.replace("-",""):
                # delete folders that don't have aggs
                for noAggs in delete_folders_no_aggs_in.split(","):
                    print("delete folders that dont have aggs: " + noAggs)
                    storage_client = storage.Client()
                    bucket = storage_client.get_bucket(src_bucket)
                    blobs = bucket.list_blobs(prefix=processing_day_path + '/' + customer + '/' + checkDay + '/' + noAggs + '/')
                    for blob in blobs:
                        blob.delete()      

                # delete folders from cje folder that are not agg folders
                print("keep the aggs for: " + checkDay)
                folderToCheck = sp.getoutput("gsutil ls gs://" + src_bucket + "/" + processing_day_path + "/" + customer + "/" + checkDay + "/cje/" + "  | awk -v FS='/' '{print $8}' ")
                for checkFolder in folderToCheck.split("\n"):
                    if 'agg' not in checkFolder:
                        print("folders to delete: " + checkFolder + " full: " + processing_day_path + '/' + customer + '/' + checkDay + '/cje/' + checkFolder)
                        storage_client = storage.Client()
                        bucket = storage_client.get_bucket(src_bucket)
                        blobs = bucket.list_blobs(prefix=processing_day_path + '/' + customer + '/' + checkDay + '/cje/' + checkFolder + '/')
                        for blob in blobs:
                            blob.delete()                         

def delete_cdrs_day_func(customer, vDate):
    output = sp.getoutput("gsutil ls gs://" + src_bucket + "/" + cdrs_path + "/" + customer + " | awk -v FS='/' '{print $7}' ")
    for checkDay in output.split("\n"):
        if checkDay!="":
            if checkDay < vDate.replace("-","")+"000000":
                print("cdrs to delete: " + checkDay + " less then: " + vDate.replace("-","")+"000000")
                storage_client = storage.Client()
                bucket = storage_client.get_bucket(src_bucket)
                blobs = bucket.list_blobs(prefix=cdrs_path + '/' + customer + '/' + checkDay)
                for blob in blobs:
                    blob.delete()                   

with DAG('dag_eod', description='DAG', schedule_interval=None, default_args=default_dag_args) as dag:    
    region = str(general_params['customer'])
    region_list = region.split(",")
    for vCustomer in region_list:
        delete_processing_day = PythonOperator(task_id='delete_processing_day_'+vCustomer, python_callable=delete_processing_day_func, op_kwargs={'customer': vCustomer, 'vDateAggs': day_delete_from_processing_aggs, 'vDateFull':day_delete_from_processing_full})
        delete_processing_day
        delete_cdrs = PythonOperator(task_id='delete_cdrs_day_'+vCustomer, python_callable=delete_cdrs_day_func, op_kwargs={'customer': vCustomer, 'vDate': day_delete_from_cdrs})
        delete_cdrs        



