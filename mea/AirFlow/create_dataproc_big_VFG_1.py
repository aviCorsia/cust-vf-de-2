import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator


working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version='$1'
#name_convention  Continual_<customer>_<controller_dag_id>
#param_prefix_name='Continual_VF_DE_final_vf_de_dataproc_cycle'
#general_params = Variable.get(param_prefix_name + '_general', deserialize_json = True)
#global variable to all gcp dags
#global_params = Variable.get('Continual_global_variable', deserialize_json = True)
global_cluster_name= 'processing-1-full-day'

general_params={
    "ENRICHMENT_JAR": "gs://vf-continual-nwp-nonlive-qa/jars/processing-enrichment-5.4.0-jar-with-dependencies.jar",
    "MOBILITY_JAR" : "gs://vf-continual-nwp-nonlive-qa/jars/processing-mobility-5.4.0-jar-with-dependencies.jar",
    "AGGS_JAR" : "gs://vf-continual-nwp-nonlive-qa/jars/processing-aggs-5.4.0-jar-with-dependencies.jar",
    "ENRICHMENT_MAIN_CLASS" : "com.continual.sonata.Processing.Enrichment.Host.Host",
    "MOBILITY_MAIN_CLASS" : "com.continual.sonata.Mobility.Processing.Host.Host",
    "AGGS_MAIN_CLASS" : "com.continual.sonata.processing.host.Host",
    "MOBILITY_PRE_PROCESS_MAIN_CLASS" : "com.continual.sonata.Mobility.PreProcessing.Host.Host",
    "CLUSTER_NAME" : "continual-mea-cluster-{{ ds_nodash }}",
    "customer" : "vf_de",
    "home_folder" : "gs://vf-continual-nwp-nonlive-qa//",
    "auto_delete_duration": 14400,
    "flow_configuration_file" : "data/configuration.json",
    "composer_bucket_name" : "europe-west3-mea-composer-47449e81-bucket",
    "working_day":"2021-02-16"
}

default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': True,
    'depends_on_past': False,
    'project_id': "vf-continual-nwp-nonlive"
}

DATAPROC_SPARK_PROP = {
    'spark.executor.instances': '4',
    'spark.executor.memoryOverhead': '10g',
    'spark.executor.cores': '25',
    'spark.executor.memory': '80g',
    'spark.master': 'yarn'
    #'spark.driver.userClassPathFirst': 'true',
    #'spark.executor.userClassPathFirst': 'true',
    #'spark.yarn.maxAppAttempts': '1'
}


with DAG(
        "create_dataproc_big_VFG_1",
        start_date=datetime.now(),
        #datetime.strptime(("2021-02-22"), "%Y-%m-%d"),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
    
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters create """ + global_cluster_name + """ \
            --bucket vf-continual-nwp-nonlive-qa \
            --region europe-west3 \
            --zone europe-west3-b \
            --tags allow-internal-dataproc-dev,allow-ssh-from-management-zone,allow-ssh-from-net-to-bastion \
            --project vf-continual-nwp-nonlive \
            --subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/dev-restricted-zone \
            --service-account vf-cont-nwp-dev-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
            --metadata enable-oslogin=true \
            --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
            --optional-components=ANACONDA,JUPYTER,ZEPPELIN \
            --enable-component-gateway \
            --no-address \
            --master-machine-type n1-standard-8 \
            --worker-machine-type n1-standard-32 \
            --num-workers 4 \
            --max-idle 1h \
            --max-age 3h
            """
    )

    create_dataproc_cluster
