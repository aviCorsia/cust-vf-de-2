import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME']) + "-monitoring"
spark_properties = """spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8, spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version='2'"""

# read runtime date
date_time_str = Variable.get('monitoring_datetime')
date_time_obj = datetime.strptime(date_time_str, '%Y-%m-%d %H:%M')
job_date = date_time_obj.strftime('%Y-%m-%d')
job_time = date_time_obj.strftime('%H:%M')

monitor_start_date = "2021-09-01"
customer = "monitoring"
hours_period = 3

def end_task_clean():
    date_time_plus = date_time_obj + timedelta(hours=hours_period)
    date_time = date_time_plus.strftime('%Y-%m-%d %H:%M')
    Variable.set('monitoring_datetime', date_time)
    print("Updated monitoring_datetime to", date_time)

    
default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

with DAG(
        "quantity_monitoring",
        start_date=days_ago(1),
        schedule_interval='0 */' + str(hours_period) + ' * * *',
        default_args=default_dag_args) as dag:
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
                gcloud beta dataproc clusters create """ + global_cluster_name + """ \
                --bucket vf-continual-nwp-nonlive-qa \
                --region europe-west3 \
                --zone """ + str(global_params['gcp_zone']) + """ \
                --tags allow-ssh-from-management-zone-qa,allow-internal-dataproc-qa,allow-ssh-from-net-to-bastion \
                --project """ + str(global_params['gcp_project']) + """ \
                --subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/qa-restricted-zone \
                --service-account vf-cont-nwp-qa-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
                --metadata enable-oslogin=true \
                --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
                --enable-component-gateway \
                --no-address \
                --image-version 1.3-debian10 \
                --master-machine-type n1-standard-4 \
                --worker-machine-type n1-standard-16 \
                --worker-boot-disk-size 100 \
                --num-secondary-workers 6 \
                --num-workers 2 \
                --max-idle 1h \
                """
    )

    quantity_monitoring = BashOperator(
        task_id='quantity_monitoring',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['VALIDATION_JAR']) + """ \
            --class=""" + str(general_params['VALIDATION_MAIN_CLASS']) + """ \
            -- customer=""" + customer + """ \
             flow=monitoring \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=""" + job_time + """ \
             job_date=""" + job_date + """ \
             monitor_start_date=""" + monitor_start_date + """ \
             monitoring_output_folder=gs://vf-continual-nwp-nonlive-qa/quantity_monitoring_acdr_september \
             type=monitoring \
             gcp=true 
        """
    )

    delete_dataproc_cluster = BashOperator(
        task_id='delete_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + """ \
            --region europe-west3 \
        """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )
    
    final_step = PythonOperator(
        task_id='final_step',
        provide_context=False,
        python_callable=end_task_clean)

    create_dataproc_cluster >> quantity_monitoring >> delete_dataproc_cluster >> final_step
