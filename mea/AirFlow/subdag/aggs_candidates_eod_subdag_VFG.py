import datetime
import os
import uuid

from airflow import DAG
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
spark_properties = 'spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=20'


def end_task_clean():
    # set finish Dag execution
    Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    # update dad execution stratus, if one task failed mark the dag as failed,
    # current_dag=get_dag()
    # current_dag.update_state()


def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


yesterday = datetime.combine(
    datetime.today() - timedelta(1),
    datetime.min.time())

default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}
def dag_process(parent_dag_name, child_dag_name, args):
    with DAG(
            dag_id='%s.%s' % (parent_dag_name, child_dag_name),
            start_date=datetime.strptime(str(Variable.get(param_prefix_name + '_' + "working_day")), "%Y-%m-%d"),
            schedule_interval="@daily",
            concurrency=64,
            default_args=default_dag_args)as dag_subdag:

        create_dataproc_cluster = BashOperator(
            task_id='create_dataproc_cluster',
            bash_command="""
                        gcloud beta dataproc clusters create """ + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                        --bucket vf-continual-nwp-nonlive-qa \
                        --region europe-west3 \
                        --zone """ + str(global_params['gcp_zone']) + """ \
                        --tags allow-ssh-from-management-zone-qa,allow-internal-dataproc-qa,allow-ssh-from-net-to-bastion \
                        --project """ + str(global_params['gcp_project']) + """ \
                        --subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/qa-restricted-zone \
                        --service-account vf-cont-nwp-qa-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
                        --metadata enable-oslogin=true \
                        --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
                        --enable-component-gateway \
                        --no-address \
                        --master-machine-type n1-standard-16 \
                        --worker-machine-type n1-standard-32 \
                        --worker-boot-disk-size 100 \
                        --master-boot-disk-size 400 \
                        --num-workers 20 \
                        --max-idle 1h \
                        --image-version 1.3-debian10 \
                        """
        )

        enrichment_agg = BashOperator(
            task_id='spark_enrichment_agg',
            bash_command="""
                       gcloud dataproc jobs submit spark \
                       --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                       --region=europe-west3 \
                       --jars=""" + str(general_params['ENRICHMENT_JAR']) + """ \
                       --class=""" + str(general_params['ENRICHMENT_MAIN_CLASS']) + """ \
                       --properties=""" + spark_properties + """ \
                       -- customer=""" + args['customer_name'] + """ \
                        flow=enrichment \
                        home_folder=""" + str(general_params['home_folder']) + """ \
                        job_id=enrichment_agg_""" + str(uuid.uuid4()) + """ \
                        job_time=00:00 \
                        job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                        bucket=day \
                        type=enrichment_agg \
                        gcp=true 
                   """,
                   trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        )

        routes_aggs = BashOperator(
            task_id='spark_routes_aggs',
            bash_command="""
                       gcloud dataproc jobs submit spark \
                       --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                       --region=europe-west3 \
                       --jars=""" + str(general_params['MOBILITY_JAR']) + """ \
                       --class=""" + str(general_params['MOBILITY_MAIN_CLASS']) + """ \
                       --properties=""" + spark_properties + """ \
                       -- customer=""" + args['customer_name'] + """ \
                        flow=routes \
                        home_folder=""" + str(general_params['home_folder']) + """ \
                        job_id=routes_aggs_""" + str(uuid.uuid4()) + """ \
                        job_time=00:00 \
                        job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                        bucket=day \
                        type=aggs \
                        gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )

        mongo_kqi = BashOperator(
            task_id='spark_mongo_kqi',
            bash_command="""
                       gcloud dataproc jobs submit spark \
                       --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                       --region=europe-west3 \
                       --jars=""" + str(general_params['AGGS_JAR']) + """ \
                       --class=""" + str(general_params['AGGS_MAIN_CLASS']) + """ \
                       --properties=""" + spark_properties + """ \
                       -- customer=""" + args['customer_name'] + """ \
                        flow=aggs \
                        home_folder=""" + str(general_params['home_folder']) + """ \
                        job_id=mongo_kqi_""" + str(uuid.uuid4()) + """ \
                        job_time=00:00 \
                        job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                        type=mongo_kqi \
                        target=hdfs \
                        gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )

        mongo_kqi_cell = BashOperator(
            task_id='spark_mongo_kqi_cell',
            bash_command="""
                       gcloud dataproc jobs submit spark \
                       --cluster=""" + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                       --region=europe-west3 \
                       --jars=""" + str(general_params['AGGS_JAR']) + """ \
                       --class=""" + str(general_params['AGGS_MAIN_CLASS']) + """ \
                       --properties=""" + spark_properties + """ \
                       -- customer=""" + args['customer_name'] + """ \
                        flow=aggs \
                        home_folder=""" + str(general_params['home_folder']) + """ \
                        job_id=mongo_kqi_cell_""" + str(uuid.uuid4()) + """ \
                        job_time=00:00 \
                        job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                        type=mongo_kqi_cell \
                        target=hdfs \
                        gcp=true 
                """,
            trigger_rule=trigger_rule.TriggerRule.ONE_SUCCESS
        )

        delete_dataproc_cluster = BashOperator(
            task_id='delete_dataproc_cluster',
            bash_command="""
                    gcloud beta dataproc clusters delete  """ + global_cluster_name + '-' + args['customer_name'].replace("_","-") + """ \
                    --region europe-west3 \
                """,
            trigger_rule=trigger_rule.TriggerRule.ALL_DONE
        )

        EOD = BashOperator(
            task_id="bash_EOD",
            bash_command='echo "End OF Day script to execute " '
        )
        create_dataproc_cluster >> enrichment_agg >> routes_aggs >> mongo_kqi >> mongo_kqi_cell >> delete_dataproc_cluster >> EOD
        return dag_subdag