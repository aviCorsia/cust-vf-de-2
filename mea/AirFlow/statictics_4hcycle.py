from airflow import DAG
from airflow.models import DagRun, Variable
from airflow.utils.dates import days_ago
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta

global_params = Variable.get('Continual_global_variable', deserialize_json=True)

# {"dag": "dataproc_4hours_cycle_VFG.de_east"}

def get_run_list(dag_id):
    dag_runs = DagRun.find(dag_id=dag_id)
    return dag_runs

def execute(dag_id):     
    dags = [
        ["cdrs_4hours_cycle_VFG", 1],
        ["dataproc_4hours_cycle_VFG", 4],
        ["aggs_candidates_eod_VFG", 1],
        ["calibrate_process_VFG", 1]
    ]
    
    days = 5
    collect = []
    for dag_id, factor in dags:
        print(">> dag_id:", dag_id)
        dag_runs = get_run_list(dag_id)[-(days * factor):]
        for run in dag_runs:
            if (run):
                # print(run.start_date, "->", run.end_date, "diff:")
                collect.append([dag_id, run.start_date, run.end_date])
            else:
                print("none!")
    
    events = sorted(collect, key=lambda i: i[1])
    buffuer = []
    for dag_id, start_date, end_date in events:      
        diff = str(end_date - start_date) if end_date else "none"
        buffuer.append(dag_id + "\t" + str(start_date) + "\t" + str(end_date) + "\t" + diff)
        
        if dag_id == dags[-1][0]:
            print("\nDay:\n" + "\n".join(buffuer))
            buffuer = []

default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

with DAG(
        "statstics_4hcycle",
        start_date=days_ago(1),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
        
    execute = PythonOperator(
        task_id='exec',
        provide_context=False,
        python_callable=execute,
        op_kwargs={"dag_id": "{{ dag_run.conf['dag'] }}"}
    )
    
    execute