import datetime
import os
import uuid
import json

from airflow import DAG
from airflow.utils import trigger_rule
from airflow.models import Variable
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

working_hour_shift = "None"
working_hour = "None"
working_date = "None"

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)
global_cluster_name = str(general_params['CLUSTER_NAME'])
spark_properties = """spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8, spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version='2'"""


def end_task_clean():
    # set finish Dag execution
    Variable.set(param_prefix_name + '_' + "is_triggered_dag_running", False)
    # update dad execution stratus, if one task failed mark the dag as failed
    # current_dag=get_dag()
    # current_dag.update_state()


def begin_tasks():
    now = datetime.now()
    print("Start run, current_date : {} ".format(datetime.strftime(now, "%Y-%m-%d_%H:%M")))
    print("Customer version: " + customer_version)


default_dag_args = {
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

with DAG(
        "cdrs_4hours_cycle_VFG",
        start_date=datetime.strptime(str(Variable.get(param_prefix_name + '_' + "working_day")), "%Y-%m-%d"),
        schedule_interval=None,
        default_args=default_dag_args)as dag:
    create_dataproc_cluster = BashOperator(
        task_id='create_dataproc_cluster',
        bash_command="""
                gcloud beta dataproc clusters create """ + global_cluster_name + """ \
                --bucket vf-continual-nwp-nonlive-qa \
                --region europe-west3 \
                --zone """ + str(global_params['gcp_zone']) + """ \
                --tags allow-ssh-from-management-zone-qa,allow-internal-dataproc-qa,allow-ssh-from-net-to-bastion \
                --project """ + str(global_params['gcp_project']) + """ \
                --subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/qa-restricted-zone \
                --service-account vf-cont-nwp-qa-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
                --metadata enable-oslogin=true \
                --properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
                --enable-component-gateway \
                --no-address \
                --master-machine-type n1-standard-8 \
                --worker-machine-type n1-standard-32 \
                --worker-boot-disk-size 200 \
                --num-workers 8 \
                --max-idle 1h \
                --image-version 1.3-debian10 \
                """
    )

    cdr_data_parsing = BashOperator(
        task_id='cdr_data_parsing',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['PARSING_JAR']) + """ \
            --class=""" + str(general_params['PARSING_MAIN_CLASS']) + """ \
            --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8 \
            -- customer=general \
             flow=cdrs \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
             job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
             type=vf_cdr_data_parser \
             duration=1440 \
             offsetDay=0 \
             gcp=true 
        """
    )
    
    cdr_voice_parsing = BashOperator(
        task_id='cdr_voice_parsing',
        bash_command="""
            gcloud dataproc jobs submit spark \
            --cluster=""" + global_cluster_name + """ \
            --region=europe-west3 \
            --jars=""" + str(general_params['PARSING_JAR']) + """ \
            --class=""" + str(general_params['PARSING_MAIN_CLASS']) + """ \
            --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8 \
            -- customer=general \
             flow=cdrs \
             home_folder=""" + str(general_params['home_folder']) + """ \
             job_id=""" + str(uuid.uuid4()) + """ \
             job_time=""" + str(Variable.get(param_prefix_name + '_' + 'working_hour_internal')) + """ \
             job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
             type=vf_cdr_voice_parser \
             duration=1440 \
             offsetDay=0 \
             gcp=true 
        """
    )
    
    acdrs_parsing = BashOperator(
        task_id='acdrs_parsing',
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE,
        bash_command="""
                gcloud dataproc jobs submit spark \
                --cluster=""" + global_cluster_name + """ \
                --region=europe-west3 \
                --jars=""" + str(general_params['PARSING_JAR']) + """ \
                --class=""" + str(general_params['PARSING_MAIN_CLASS']) + """ \
                --properties=spark.executor.memoryOverhead=10g,spark.executor.cores=25,spark.executor.memory=80g,spark.executor.instances=8 \
                -- customer=general \
                 flow=acdrs \
                 home_folder=""" + str(general_params['home_folder']) + """ \
                 job_id=""" + str(uuid.uuid4()) + """ \
                 job_time=02:00 \
                 job_date=""" + str(Variable.get(param_prefix_name + '_' + 'working_date_internal')) + """ \
                 type=vf_acdr_parser \
                 duration=360 \
                 offsetDay=0 \
                 gcp=true 
            """
    )

    delete_dataproc_cluster = BashOperator(
        task_id='delete_dataproc_cluster',
        bash_command="""
            gcloud beta dataproc clusters delete  """ + global_cluster_name + """ \
            --region europe-west3 \
        """,
        trigger_rule=trigger_rule.TriggerRule.ALL_DONE
    )

    final_step = PythonOperator(
        task_id='final_step',
        provide_context=False,
        python_callable=end_task_clean)

    first_step = PythonOperator(
        task_id='first_step',
        provide_context=False,
        python_callable=begin_tasks
    )

    # first_step >> time_shift_calc >> create_dataproc_cluster >> enrichment_job1 >> enrichment_job2 >> prepare_routes >> routes >> counters >> routes_counters >> delete_dataproc_cluster >> final_step
    # first_step >> create_dataproc_cluster >> delete_dataproc_cluster >> final_step
    first_step >> create_dataproc_cluster >> cdr_data_parsing >> cdr_voice_parsing >> acdrs_parsing >> delete_dataproc_cluster >> final_step
