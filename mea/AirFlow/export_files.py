from airflow import DAG, AirflowException
from airflow.models import Variable
from airflow.contrib.operators.gcs_to_gcs import GoogleCloudStorageToGoogleCloudStorageOperator
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago
from google.cloud import storage
from airflow.operators.python_operator import PythonOperator

customer_version = '$1'
# name_convention  Continual_<customer>_<controller_dag_id>
param_prefix_name = 'Continual_VF_DE_final_vf_de_dataproc_cycle'
general_params = Variable.get(param_prefix_name + '_general', deserialize_json=True)
# global variable to all gcp dags
global_params = Variable.get('Continual_global_variable', deserialize_json=True)

# read runtime date
days_to_subtruct = 1
working_day_str = str(Variable.get(param_prefix_name + '_' + "working_day"))
date_time_str = datetime.strptime(working_day_str, "%Y-%m-%d") - timedelta(days=days_to_subtruct)
job_date = date_time_str.strftime('%Y-%m-%d')

default_dag_args = {
    'email_on_failure': True,
    'email_on_retry': False,
    'email_on_success': True,
    'email': ['vadim.zinchuk@continualexperience.com'],
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'wait_for_downstream': False,
    'depends_on_past': False,
    'project_id': global_params['gcp_project']
}

job_year = date_time_str.strftime('%Y')
job_month = date_time_str.strftime('%m').lstrip('0')
job_day = date_time_str.strftime('%d').lstrip('0')

src_bucket = "vf-continual-nwp-nonlive-qa"
dag_list = []

storage_client = storage.Client()
bucket = storage_client.bucket(src_bucket)
bucket_str = str(bucket)

def check_exists(name):
    exists = storage.Blob(bucket=bucket, name=name).exists(storage_client)
    if not exists:
        msg = "_SUCCESS file was not copied, source path: " + name + ", bucket: " + bucket_str
        print(msg)
        raise AirflowException(msg)
    else:
        print("_SUCCESS file was found, source path: " + name + ", bucket: " + bucket_str)

def append_dag(name, source, target, extension, dag_list):
    source_with_ext = source + "*." + extension
    print(">>", name, ";", source_with_ext, ";", target)
    
    dag = GoogleCloudStorageToGoogleCloudStorageOperator(
        task_id=name,
        source_bucket=src_bucket,
        source_object=source_with_ext,
        destination_bucket=src_bucket,
        destination_object=target
    )
    
    if 'copy_gis_' in name:
        dag_list.append(dag)
    else:
        success_dag = GoogleCloudStorageToGoogleCloudStorageOperator(
            task_id=name + '_success',
            source_bucket=src_bucket,
            source_object=source + '_SUCCESS',
            destination_bucket=src_bucket,
            destination_object=target + '_SUCCESS'
        )
        
        check_success = PythonOperator(
            task_id=name + '_check_exists',
            provide_context=False,
            python_callable=check_exists,
            op_kwargs={'name': target + '_SUCCESS'}
        )
        
        dag_list.append(dag >> success_dag >> check_success)

with DAG(
        "export_files",
        start_date=days_ago(1),
        schedule_interval=None,
        default_args=default_dag_args) as dag:

    region = str(general_params['customer'])
    region_list = region.split(",")
    for customer in region_list:
        root_src = "data/" + customer
        root_dst = "data/export/system=" + customer
        date_path = "year={}/month={}/day={}/".format(job_year, job_month, job_day)
        root_date = root_src + "/" + job_date

        append_dag('copy_road_counters_' + customer, root_date + "/aggs/mongo_way_kqi/", root_dst + "/type=road_counters/" + date_path, "json", dag_list)
        append_dag('copy_road_cell_counters_' + customer, root_date + "/aggs/mongo_way_cell_kqi/", root_dst + "/type=road_cell_counters/" + date_path, "json", dag_list)
        append_dag('copy_impact_counters_' + customer, root_date + "/cje/impact_counters_day_agg/", root_dst + "/type=impact_counters/" + date_path, "parquet", dag_list)
        append_dag('copy_cell_counters_' + customer, root_date + "/cje/cell_counters_day_agg/", root_dst + "/type=cell_counters/" + date_path, "parquet", dag_list)
        append_dag('copy_gis_' + customer, root_src + "/gis/", root_dst + "/type=gis/" + date_path, "txt", dag_list)
        
    dag_list