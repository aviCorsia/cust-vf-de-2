# Base modules
from datetime import datetime, timedelta
from calendar import monthrange
import json
import os

# Airflow modules
from airflow import DAG, configuration
from airflow.contrib.operators.dataproc_operator import DataprocClusterDeleteOperator
from airflow.contrib.operators import dataproc_operator as dpo
from airflow.operators import bash_operator
from airflow.models import Variable
from airflow.utils.trigger_rule import TriggerRule

''' ====================================== Config Settings ====================================== '''

# Take last day of year/month target (last month)
today = datetime.today().date()
first = today.replace(day=1)
date_target = first - timedelta(days=1)

year = date_target.year
month = date_target.month

month_str = str(format(int(month), '02d'))

# Initialize input path
use_case_name = "ClusterSupportCreation"
use_case_version = "0.1"

# Schedule and cluster settings
earlier = datetime.combine(datetime.today() - timedelta(minutes=5), datetime.min.time())
cluster_name = 'support-cluster-{}-target-{}{}'.format(datetime.now().strftime("%Y%m%d"), str(year), month_str)

# Job arguments
JOB_ARGS = {
    "gcp_project": 'Continual-RND',
    "DAG_version": "0.1",
}

# Airflow parameters
DEFAULT_DAG_ARGS = {
    'owner': 'airflow',  # The owner of the task
    'depends_on_past': False,  # Task instance should not rely on the previous task's schedule to succeed
    'start_date': earlier,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,  # Retry once before failing the task
    'retry_delay': timedelta(minutes=5),  # Time between retries
    'project_id': JOB_ARGS['gcp_project']  # Cloud Composer project ID
}

'''================================== Templated Batch Commands ================================== '''

# Batch command to create Dataproc cluster
templated_create_command_dataproc = """
gcloud beta dataproc clusters create vf-de-continual-medium-4-workers \
--bucket vf-continual-nwp-nonlive-qa \
--region europe-west3 \
--zone europe-west3-b \
--subnet projects/vf-continual-nwp-nonlive/regions/europe-west3/subnetworks/qa-restricted-zone \
--tags allow-ssh-from-management-zone-qa,allow-internal-dataproc-qa,allow-ssh-from-net-to-bastion \
--project vf-continual-nwp-nonlive \
--service-account vf-cont-nwp-qa-dp-ds-sa@vf-continual-nwp-nonlive.iam.gserviceaccount.com \
--master-machine-type n1-standard-4 \
--num-secondary-workers 2 \
--num-workers 2 \
--worker-boot-disk-size 100 \
--worker-machine-type n1-standard-16 \
--max-idle 2h \
--metadata enable-oslogin=true \
--properties core:fs.gs.implicit.dir.repair.enable=false,core:fs.gs.status.parallel.enable=true \
--optional-components=JUPYTER \
--enable-component-gateway \
--no-address
"""

'''===================================== DAG Implementation ===================================== '''

# Batch command to submit spark job into the Dataproc cluster
templated_create_spark_job_dataproc = """
gcloud dataproc jobs submit spark --cluster=uat-test-continual-v3 \
--region=europe-west3 \
--jars gs://vf-continual-nwp-nonlive-qa/vf_parser_test_1/jar/processing-parser-5.4-VF-TEST-SNAPSHOT-jar-with-dependencies.jar \
--class com.continual.sonata.processing.parser.host.Host \
--conf spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version='2' \
-- customer=vf_de_test flow=parser home_folder=gs://vf-continual-nwp-nonlive-qa/vf_parser_test_1/ job_id=f6257780-7f88-41df-a5ff-9156cf768fe0 job_time=04:00 job_date=2020-06-13 type=vf_cdr_parser gcp=true
"""

#-- customer=vf_de_test flow=parser home_folder=gs://vf-continual-nwp-nonlive-qa/vf_parser_test_1/ job_id=f6257780-7f88-41df-a5ff-9156cf768fe0 job_time=04:00 job_date=2020-06-13 type=vf_cdr_parser gcp=true

'''===================================== DAG Implementation ===================================== '''


# Create Directed Acyclic Graph for Airflow
DAG_name = '{}.{}_{}'.format(use_case_name, use_case_version, JOB_ARGS['DAG_version'])
schedule_interval = None

DATAPROC_SPARK_PROP = {
    'spark.executor.instances': '3',
    'spark.executor.memoryOverhead': '10g',
    'spark.executor.cores': '20',
    'spark.executor.memory': '80g',
    'spark.master': 'yarn'
    #'spark.driver.userClassPathFirst': 'true',
    #'spark.executor.userClassPathFirst': 'true',
    #'spark.yarn.maxAppAttempts': '1'
}  # Dict mentioning Spark job's properties

with DAG('vf_de_dataproc_create_medium_4_workers_n1_16',  start_date=datetime.now(),default_args=DEFAULT_DAG_ARGS, schedule_interval=schedule_interval) as dag:
    # Create Cloud Dataproc cluster
    create_cluster = bash_operator.BashOperator(
        task_id='create_dataproc_cluster',
        bash_command=templated_create_command_dataproc
    )

    # Trigger DAG
    create_cluster