--liquibase formatted sql
--changeset admin:insert_vf_de_markets
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (1,'Nord',1,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (2,'Nord_West',2,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (3,'Ost',3,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (4,'Sud',4,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (5,'Sud_West',5,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (6,'Nord_Ost',6,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (7,'Rhein_Main',7,0);
INSERT INTO public.market (market_id,market_name,"type",parent_polygon_id) VALUES (8,'West',8,0);




