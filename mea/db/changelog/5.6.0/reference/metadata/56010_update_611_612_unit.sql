--
-- PostgreSQL database dump
-- changeset admin 56010_update_611_612_unit.sql

--
-- Data for Name: kqi_catalog; Type: TABLE DATA; Schema: public; Owner: postgres
--
UPDATE kqi_catalog set unit_id= 1 where id =611;
UPDATE kqi_catalog set unit_id= 1 where id =612;

--
-- Data for Name: kqi_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--
UPDATE kqi_metadata set unit=1 where kqi_id = 611;
UPDATE kqi_metadata set unit=1 where kqi_id = 612;