--
-- PostgreSQL database dump
-- changeset admin 56009_enable_611_612.sql

--
-- Data for Name: kqi_catalog; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.kqi_catalog (id, name, description, category_id, unit_id) VALUES (611, 'DL User Throughput Events', 'DL User Throughput Events', 22, 5);
INSERT INTO public.kqi_catalog (id, name, description, category_id, unit_id) VALUES (612, 'UL User Throughput Events', 'UL User Throughput Events', 22, 5);

--
-- Data for Name: kqi_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (611, 'DL User Throughput Events', 'DL User Throughput Events', 'Overview', true, 'Total', 8000, 22, 5);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (611, 'DL User Throughput Events', 'DL User Throughput Events', 'Map', false, 'Total', 8000, 22, 5);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (611, 'DL User Throughput Events', 'DL User Throughput Events', 'Dashboard', false, 'Total', 8000, 22, 5);

INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (612, 'UL User Throughput Events', 'UL User Throughput Events', 'Overview', true, 'Total', 8000, 22, 5);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (612, 'UL User Throughput Events', 'UL User Throughput Events', 'Map', false, 'Total', 8000, 22, 5);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (612, 'UL User Throughput Events', 'UL User Throughput Events', 'Dashboard', false, 'Total', 8000, 22, 5);