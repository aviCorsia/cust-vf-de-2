--
-- Data for Name: kqi_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

UPDATE kqi_metadata set active=true WHERE kqi_id = 500 and service = 'Map';
UPDATE kqi_metadata set kqi_name = 'Call Count', kqi_description = 'Call_Count' where kqi_id =500;
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (500, 'Call Count', 'Call Count', 'Dashboard', true, 'Total', 8000, 15, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (500, 'Call Count', 'Call Count', 'Overview', true, 'Total', 8000, 15, 1);


UPDATE kqi_metadata set active=false WHERE kqi_id = 501; 

