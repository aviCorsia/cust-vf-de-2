--
-- Data for Name: kqi_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

UPDATE kqi_metadata set active = 'true' where kqi_id in (2100, 2101, 2103, 2002, 2003, 2000, 2001, 2204, 2205, 2005, 2200, 2201, 2222, 2202, 2203, 2223, 2216, 2217, 2214, 2224, 2218, 2219, 2225, 5003, 5004, 5005);

update public.kqi_metadata set kqi_name= 'RRC Low SINR PUCCH/PUSCH', kqi_description= 'RRC Low SINR PUCCH/PUSCH' where kqi_id = '2103';
update public.kqi_metadata set kqi_name= 'RRC Long Call Setup Time', kqi_description= 'RRC Long Call Setup Time' where kqi_id = '2002';
update public.kqi_metadata set kqi_name= 'RRC Long Data Setup Time', kqi_description= 'RRC Long Data Setup Time' where kqi_id = '2003';
update public.kqi_metadata set kqi_name= 'RRC IRAT Call Failures', kqi_description= 'RRC IRAT Call Failures' where kqi_id = '2216';
update public.kqi_metadata set kqi_name= 'RRC IRAT Data Failures', kqi_description= 'RRC IRAT Data Failures' where kqi_id = '2217';
update public.kqi_metadata set kqi_name= 'RRC CSFB Call Failures', kqi_description= 'RRC CSFB Call Failures' where kqi_id = '2214';
