--
-- PostgreSQL database dump
-- changeset admin 56013_disable_avarget_rrc_rate.sql

--
-- Data for Name: kqi_metadata; Type: METADATA; Schema: public; Owner: postgres
--


UPDATE public.kqi_metadata set active = 'false' where kqi_id in (2108, 2109, 2110) and service in ('Map', 'Dashboard');

UPDATE public.kqi_metadata set active = 'true' where kqi_id in (2108, 2109, 2110) and service = 'Overview';