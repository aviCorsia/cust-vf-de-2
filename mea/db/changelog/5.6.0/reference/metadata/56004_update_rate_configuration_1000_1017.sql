
--
-- Data for Name: kqi_catalog; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.kqi_catalog (id, name, description, category_id, unit_id) VALUES (100001, 'Voice_Drops', 'Voice Drops', 1, 1);
INSERT INTO public.kqi_catalog (id, name, description, category_id, unit_id) VALUES (100004, 'Voice_Drops_Zero_Duration', 'Voice Drops Zero Duration', 1, 1);
INSERT INTO public.kqi_catalog (id, name, description, category_id, unit_id) VALUES (100501, 'End_Calls', 'End calls', 6, 1);
INSERT INTO public.kqi_catalog (id, name, description, category_id, unit_id) VALUES (100506, 'End_Call_Zero_Duration', 'End Call Zero Duration Time', 6, 1);


--
-- Data for Name: kqi_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100001, 'Dropped Calls (Duration > 0)', 'Dropped Calls (Duration > 0)', 'Dashboard', false, 'Total', 8000, 12, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100001, 'Dropped Calls (Duration > 0)', 'Dropped Calls (Duration > 0)', 'Map', true, 'Total', 8000, 12, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100001, 'Dropped Calls (Duration > 0)', 'Dropped Calls (Duration > 0)', 'Overview', true, 'Total', 8000, 12, 1);

INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100004, 'Dropped Calls (Duration = 0)', 'Dropped Calls (Duration = 0)', 'Dashboard', false, 'Total', 8000, 12, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100004, 'Dropped Calls (Duration = 0)', 'Dropped Calls (Duration = 0)', 'Map', true, 'Total', 8000, 12, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100004, 'Dropped Calls (Duration = 0)', 'Dropped Calls (Duration = 0)', 'Overview', true, 'Total', 8000, 12, 1);

INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100501, 'Call Count (Duration > 0)', 'Call Count (Duration > 0)', 'Dashboard', false, 'Total', 8000, 15, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100501, 'Call Count (Duration > 0)', 'Call Count (Duration > 0)', 'Map', true, 'Total', 8000, 15, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100501, 'Call Count (Duration > 0)', 'Call Count (Duration > 0)', 'Overview', true, 'Total', 8000, 15, 1);

INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100506, 'Call Count (Duration = 0)', 'Call Count (Duration = 0)', 'Dashboard', false, 'Total', 8000, 15, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100506, 'Call Count (Duration = 0)', 'Call Count (Duration = 0)', 'Map', true, 'Total', 8000, 15, 1);
INSERT INTO public.kqi_metadata (kqi_id, kqi_name, kqi_description, service, active, calc_type, element_type, category, unit) VALUES (100506, 'Call Count (Duration = 0)', 'Call Count (Duration = 0)', 'Overview', true, 'Total', 8000, 15, 1);

UPDATE kqi_metadata set active = 'false' where kqi_id in (1, 4, 501, 506);
UPDATE kqi_metadata set active=true WHERE kqi_id = 406 AND service = 'Map';

--
-- Data for Name: kqi_metadata_layers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100027, 100001, 0.979999999999999982, 1, 'DARK_RED', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100026, 100001, 0.949999999999999956, 0.979999999999999982, 'Red', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100025, 100001, 0.800000000000000044, 0.949999999999999956, 'Orange', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100024, 100001, 0, 0.800000000000000044, 'Yellow', 'Map', NULL);


INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100035, 100004, 0.979999999999999982, 1, 'DARK_RED', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100034, 100004, 0.949999999999999956, 0.979999999999999982, 'Red', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100033, 100004, 0.800000000000000044, 0.949999999999999956, 'Orange', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100032, 100004, 0, 0.800000000000000044, 'Yellow', 'Map', NULL);


INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100164, 100501, 0, 0.800000000000000044, 'Blue1', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100165, 100501, 0.800000000000000044, 0.949999999999999956, 'Blue2', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100166, 100501, 0.949999999999999956, 0.979999999999999982, 'Blue3', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100167, 100501, 0.979999999999999982, 1, 'Blue4', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100533, 100501, 0, 0.800000000000000044, 'COMPARE-TRAFFIC-POS-1', 'Map', 'Increase');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100534, 100501, 0.800000000000000044, 0.949999999999999956, 'COMPARE-TRAFFIC-POS-2', 'Map', 'Increase');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100535, 100501, 0.949999999999999956, 1, 'COMPARE-TRAFFIC-POS-3', 'Map', 'Increase');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100583, 100501, 0, 0.800000000000000044, 'COMPARE-TRAFFIC-NEG-1', 'Map', 'Decrease');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100584, 100501, 0.800000000000000044, 0.949999999999999956, 'COMPARE-TRAFFIC-NEG-2', 'Map', 'Decrease');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100585, 100501, 0.949999999999999956, 1, 'COMPARE-TRAFFIC-NEG-3', 'Map', 'Decrease');


INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100172, 100506, 0, 0.800000000000000044, 'Blue1', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100173, 100506, 0.800000000000000044, 0.949999999999999956, 'Blue2', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100174, 100506, 0.949999999999999956, 0.979999999999999982, 'Blue3', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100175, 100506, 0.979999999999999982, 1, 'Blue4', 'Map', NULL);
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100541, 100506, 0, 0.800000000000000044, 'COMPARE-TRAFFIC-POS-1', 'Map', 'Increase');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100542, 100506, 0.800000000000000044, 0.949999999999999956, 'COMPARE-TRAFFIC-POS-2', 'Map', 'Increase');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100543, 100506, 0.949999999999999956, 1, 'COMPARE-TRAFFIC-POS-3', 'Map', 'Increase');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100589, 100506, 0, 0.800000000000000044, 'COMPARE-TRAFFIC-NEG-1', 'Map', 'Decrease');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100590, 100506, 0.800000000000000044, 0.949999999999999956, 'COMPARE-TRAFFIC-NEG-2', 'Map', 'Decrease');
INSERT INTO public.kqi_metadata_layers (id, kqi_id, from_value, to_value, color, service, compare_mode) VALUES (100591, 100506, 0.949999999999999956, 1, 'COMPARE-TRAFFIC-NEG-3', 'Map', 'Decrease');



--
-- Data for Name: kqi_rate; Type: TABLE DATA; Schema: public; Owner: postgres
--

UPDATE kqi_rate set counter_id =100001, denominator_id=100501 where id =1000;
UPDATE kqi_rate set counter_id =100004, denominator_id=100506 where id =1017;



