--
-- Data for Name: kqi_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--
UPDATE kqi_metadata set active = 'true' where kqi_id in (506, 501) and service in ('Map', 'Overview');
UPDATE kqi_metadata set active = 'false' where kqi_id in (100506, 100501) and service in ('Map', 'Overview');

