#!/bin/sh

# -------------------------------------------------------
# Local Start Script
# -------------------------------------------------------
if [[ -z "${MEA_HOME}" ]]; then
  echo "enviroment variable MEA_HOME dosen't exist"
  exit 1
fi
now=$(date +"%m-%d-%Y-%T")
#configuration #
PROPERTY_FILE=${MEA_HOME}/mea/repo/db_migration.properties
if [[ -z "${PROPERTY_FILE}" ]]; then
  echo "propery file dosen't exist"
  exit 1
fi

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}
db_username=$(getProperty "db_username")
db_password=$(getProperty "db_password")
db_host=$(getProperty "db_host")
port=$(getProperty "port")
url_ext=$(getProperty "url_ext")
contexts=$(getProperty "contexts")
db_url=jdbc:postgresql://${db_host}
changelog_file_ext=.linux.xml
service_path=${MEA_HOME}/mea/services/db_migration
log_path=${MEA_HOME}/mea/logs/db_migration
jar_location=${MEA_HOME}/mea/services/db_migration/lib
file_name=${MEA_HOME}/mea/bin/schemas_names.txt
#change directory to service path #
cd ${service_path}
while IFS=  read -r line || [[ -n "$line" ]] 
do
	    name=$line
        echo "DB INSTANCE - $name"
java -jar  ${jar_location}/liquibase.jar \
  --driver=org.postgresql.Driver \
  --classpath="${jar_location}/postgresql-42.2.18.jar" \
  --url="${db_url}:${port}/$name${url_ext}" \
  --changeLogFile="db/changelog/de_changelog-master-${name}${changelog_file_ext}" \
  --username=${db_username} \
  --password=${db_password} \
  --logLevel=Debug \
  --logFile=${log_path}/liquibase_"${name}"_"${now}".log  \
  --contexts="${contexts}" \
 update
echo ==========================================================================
done < ${file_name}








